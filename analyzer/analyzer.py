# -*- coding: utf-8 -*-
'''
Created on 29.02.2012

@author: natalia
'''
from sympy import Symbol, simplify, Float
import re
import math

class Analyzer(object):

    '''
    проверяет на правильность шаг решения, введенный студентом с помощью пользовательского интерфейса,
    с помощью одного из заложенных в системе решений задачи, наиболее близкого к решению студента
    '''

    def __init__(self):
        
        '''
        Constructor
     
        '''   
    def break_into_number_and_signs(self, st):
        '''
        эта функция получает на вход строки вида "+3-" и возвращает результат типа [+, 3, -]
        Нужна для приведения выражения в приемлемый для Sympy вид
        '''
        left_sign = ''
        right_sign = ''
           
        #если первый знак не цифра
        if ((st[0] != '0') and (st[0] != '1') and (st[0] != '2') and (st[0] != '3') and (st[0] != '4')
            and (st[0] != '5') and (st[0] != '6') and (st[0] != '7') and (st[0] != '8') and (st[0] != '9')):
           
            left_sign = st[0]
            st = st[1:]
               
        #если последний знак не цифра
        if ((st[len(st)-1] != '0') and (st[len(st)-1] != '1') and (st[len(st)-1] != '2') and (st[len(st)-1] != '3') and (st[len(st)-1] != '4')
            and (st[len(st)-1] != '5') and (st[len(st)-1] != '6') and (st[len(st)-1] != '7') and (st[len(st)-1] != '8') and (st[len(st)-1] != '9')):
           
            right_sign = st[len(st)-1]
            st = st[:-1]
           
        return (left_sign, st, right_sign)
    
    def number_of_equals(self,expression):
        ''' 
        вспомогательная функция для автоматического определения типа шага, введенного студентом. Возвращает количество знаков равенства в выражении
        '''
        return len(expression.split('=')) - 1
    
    def met_notations(self, expression):
        '''
        вспомогательная функция для автоматического определения типа шага, введенного студентом. Возвращает список обозначений, присутствующих в выражении
        '''
        expression = self.remove_operation_names(expression)
        result = []
        for i in range(len(self.__notation)):
            if (expression.find(self.__notation[i]) != -1):
                result.append(self.__notation[i])
                expression = expression.replace(self.__notation[i], '')
        return result
    
    def dict_are_same(self, list1, list2):
        '''
        вспомогательная функция для автоматического определения типа шага, введенного студентом. Определяет - совпадает ли состав у двух списков
        '''
        if (len(list1) != len(list2)):
            return 0
        else:
            intersect = []
            for i in range(len(list1)):
                if list1[i] in list2:
                    intersect.append(list1[i])
            if (len(intersect) == len(list1)):
                return 1
            else:
                return 0
            
    def dict_simil(self, list1, list2):
        '''
        вспомогательная функция для автоматического определения типа шага, введенного студентом. Определяет - совпадает ли состав у двух списков
        '''
        intersect = []
        for i in range(len(list1)):
            if ((list1[i] in list2) and not (list1[i] in intersect)):
                intersect.append(list1[i])
        for i in range(len(list2)):
            if ((list2[i] in list1) and not (list2[i] in intersect)):
                intersect.append(list2[i])
        return len(intersect)
            
    def dict_diff(self, list1, list2):
        '''
        Определяет разницу в составе двух списков
        '''
        difference = []
        for i in range(len(list1)):
            if (not (list1[i] in list2) and not(list1[i] in difference)):
                difference.append(list1[i])
        for i in range(len(list2)):
            if (not (list2[i] in list1) and not(list2[i] in difference)):
                difference.append(list2[i])
        return len(difference)
    
    def type_selection_is_candidate(self, expression, pattern, notations):  
        '''
        вспомогательная функция для автоматического определения типа шага, введенного студентом. Возвращает 1, если у сравниваемых выражений
        совпадает число знаков равенства и присутствующие обозначения задачи
        '''
        self.set_notation(notations)
        expression = expression.replace('#almost#', '')
        expression = expression.replace('#fixed#', '')          
        pattern = pattern.replace('#almost#', '')
        pattern = pattern.replace('#fixed#', '')   
        if (self.number_of_equals(expression) == self.number_of_equals(pattern)):
            if (self.dict_are_same(self.met_notations(expression), self.met_notations(pattern))):
                return 1
            else:
                return 0
        else:
            return 0  
                   
    def convert_to_rational_symbolic(self, expression):
        '''
        данная функция из выражения вида 2/3*x + 4/5*y делает выражение Rational(2,3)*x + Rational(4,5)*y, приводя выражение, 
        введенное студентом в соответствие с требуемым синтаксисом SymPy
        '''
        list_of_fractions = re.findall( r'[(-+*\s=\^]?[0-9]{1,}[/]{1}[0-9]{1,}[-+*)\s=\^]?', expression)
        
        for i in range(len(list_of_fractions)):
            
            st = list_of_fractions[i]            
            st_list = self.break_into_number_and_signs(st)
          
            num, denom = st_list[1].split('/')
            number = 'Rational(' + num + ',' + denom + ')'
            
            expression = expression.replace(list_of_fractions[i], st_list[0] + number + st_list[2]) 

        return expression
    
    def remove_operation_names(self, expression):
        '''эта функция нужна и для проверки на эквивалентность, и для сверки списка обозначений '''
                #обработка кодировки
        expression = unicode(expression).encode('utf-8')

        #убираем все лишние пробелы
        expression = expression.strip(' \t\n')
        expression = expression.replace(' ', '')

        # ========== преобразование для суммы =====================
        reObj = re.compile(r'(sum\{.*?\}\{.*?\})')
        list_of_sums = reObj.findall(expression)
        for i in range(len(list_of_sums)):
            out = list_of_sums[i].strip(' \t\n')
            out = out.replace('sum{', '').replace('=', ';').replace('}{', ';').replace('}', '')
            out = out.split(';')
            if(len(out) != 4):
                continue
            try:
                int_from = int(out[1])
                int_to = int(out[2])
            except:
                continue
            if(abs(int_to - int_from) > 10):
                continue
            if(int_from > int_to):
                int_d = -1
            else:
                int_d = 1
            if((int_to > 0) or (int_to == 0 and int_d > 0)):
                int_to = int_to + 1
            else:
                int_to = int_to - 1
            sums = ''
            for j in range(int_from, int_to, int_d):
                if(sums != ''):
                    sums = sums + '+'
                sums = sums + out[3].replace(out[0], '%s' % j)
            expression = expression.replace(list_of_sums[i], '(' + sums + ')')
        # ========== END преобразование для суммы =====================

        #убираем sqrt
        reObj = re.compile(r'sqrt\{(.*?)\}')
        list_of_roots = reObj.findall(expression)
        for i in range(len(list_of_roots)):
            expression = expression.replace('sqrt{' + list_of_roots[i] + '}', '(' + list_of_roots[i] + ')^0.5')

        #убираем frac
        reObj = re.compile(r'frac\{(.*?)\}\{(.*?)\}')
        list_of_fracs = reObj.findall(expression)
        for i in range(len(list_of_fracs)):
            num = list_of_fracs[i][0]
            denum = list_of_fracs[i][1]
            expression = expression.replace('frac{' + num + '}{' + denum + '}', '(' + num + ')' + '/' + '(' + denum + ')')
              
        #преобразование синтаксиса для матриц
        expression = expression.replace('][', ']*[')
        reObj = re.compile(r'\[(.*?)\]')
        list_of_roots = reObj.findall(expression)
        for i in range(len(list_of_roots)):
            out = list_of_roots[i].strip(' \t\n')
            out = out.replace(';', '],[')
            expression = expression.replace('[' + list_of_roots[i] + ']', 'Matrix([[' + out + ']])')
            
        #избавляемся от знака плюс-минус
        expression = expression.replace('\pm', '+')
            
        return expression
            
    def make_readable_for_sympy(self, expression, replace_equals_sign = False, replace_notation = True):
        
        expression = self.remove_operation_names(expression)
               
        #экранируем переменные и расставляем забытые умножения
        if (replace_notation == True):
            expression = self.replace_notation(expression)
            
        #возведение в степень
        expression = expression.replace("^","**")
            
        #теперь заменяем знак корня. раньше нельзя, иначе буквы из замены могут быть приняты за обозначения
        expression = expression.replace('**0.5', '**Rational(1,2)')
        expression = self.convert_to_rational_symbolic(expression)
        
        # Так как в справке по SymPy написано, что The equals sign (=) is the assignment operator, not an equality. 
        #If you want to do x=y, use Eq(x,y) for equality. При сверке на эквивалентность знак равенства нужен,
        #по нему выражение разбивается на две части, а при вычислении количества операторов в выражении - не нужен
        exp_parts = expression.split('=')
        if len(exp_parts) > 1 & replace_equals_sign == True:
            exppart1 = exp_parts[0]
            exppart2 = exp_parts[1]
            expression ='Eq(' + exppart1 + ',' + exppart2 + ')'
        
        return expression

    def set_notation (self, notations):
        '''
        Установка текущего списка переменных, чтобы не таскать их из фцнкции в функцию.
        Сортировка списка от длинных переменных к коротким, чтобы избежать возможного пересечения при замене.
        '''
        self.__notation = notations
        self.__notation.sort(key=self.sort_length, reverse=True)

    def sort_length(self, input_str):
        '''
        Используется для сортировки перменных по длине
        '''
        return len(input_str)
     
    def replace_notation (self, expression):
        '''
        Заменяем все переменный на переменные вида x1y, чтобы избежать возможной ошибки из-за некоректого символа и облегчить расстановку пропущенных умножений.
        Расставляем пропущенные знаки умножения.
        '''
        #служебные слова временно заменяем на другие, чтобы не было пересечения по символам, а после обработки по всем переменным, вернём обратно
        dic = {'#1#' : 'Rational', '#2#' : 'Matrix', '#3#' : '#fixed#', '#4#' : '#almost#'}
        for key, value in dic.items():
            expression = expression.replace(value, key)
        j = 0
        for i in range(len(self.__notation)):
            j = j + 1
            var = 'x%sy' % j
            expression = expression.replace(self.__notation[i], var)

        #умножение между переменной и переменной или скобочкой
        expression = expression.replace('yx', 'y*x').replace('y(', 'y*(').replace(')x', ')*x')
        #умножение между цифрой и переменной или открывающейся скобочкой
        r = re.compile(r'(\d)([x\(])')
        expression = r.sub(r'\1*\2', expression)
        for key, value in dic.items():
            expression = expression.replace(key, value)
        return expression
    
    def op_diff(self, pattern, expression):
        difference = 0
        op_arr = ['-', '^', '*', '+']
        for i in range (len(op_arr)):
            difference = difference + math.fabs(pattern.count(op_arr[i]) - expression.count(op_arr[i]))           
        return difference
    
    def set_notations(self, notations):
        '''иногда вызываются отдельные функции анализатора, помимо проверки выражений, в которых требуются обозначения'''
        self.set_notation(notations)
    
    def val_diff(self, pattern, expression):
        difference = 0
        pat_nots = self.met_notations(pattern)
        exp_nots = self.met_notations(expression)
        exp_nums = self.get_numbers(expression)
        pat_nums = self.get_numbers(pattern)
        difference = self.dict_diff(pat_nots, exp_nots) + self.dict_diff(exp_nums, pat_nums)
        return difference
        
    def get_numbers(self, expression):
        list_of_numbers = re.findall( r'[(-+*\s=]?[0-9./]{1,}[-+*)\s=\^]?', expression)  
        result = []      
        for i in range(len(list_of_numbers)):
            result.append(list_of_numbers[i])
        return result           
              
    def check_expression (self, pattern, expression, check_type):
        '''
        Сравниваем два выражения не содержащих равенство, например 2*(a+b) и 2*b+2*a.
        Замечание: simplify для матриц работает не так как для обычных строк, в частности, не упрощает сложные выражения
        '''
        #временный патч
        pattern = pattern.replace('#almost#', '')
        pattern = pattern.replace('#fixed#', '') 

        dif = Symbol('dif')
        try:
            dif = simplify(pattern + '- (' + expression + ')')
        except:
            return 0
        #если результат матрица
        #todo: округление для матриц
        if(dif.is_Matrix):
            #проверка на то, что матрица нулевая, не нашёл стандартного способа
            r = re.compile(r'[\s\[\],0]')
            res = r.sub('', '%s' % dif)
            if(res == ''):
                return 1            
            return 0
        #округление для случая небольшого несоответствия
        accuracy = 2
        dif = simplify(dif.subs([(n, round(n, accuracy)) for n in dif.atoms(Float)]))
        if(dif == 0):
            return 1
        else:
            #это для случаев, когда студент может ввести как \sqrt{66}, так и 8.128, округление выше в этих случаях не работает
            try:
                if (abs(round(dif,2))==0):
                    return 1  
            except:
                return 0                 
        return 0
    
    # для проверки выражений типа |c|=\sqrt{(c,c)} и (|c|)^2=(c,c) 
    def remove_sqrt(self, expression):
        remove = False
        if (expression.find('sqrt') != -1):
            remove = True
        expression = expression.split('=')
        if (remove):
            expression[0] = '(' + expression[0] + ')**2'
            expression[1] = '(' + expression[1] + ')**2'
            
        result = expression[0] + '=' + expression[1]
        return result
    
    def check_equivalence(self, notations, step_hard_data, milestone_hard_data, check_type = 0): 
        '''
        Сравниваем два выражения: введёное студентом и заложенное в системе. 
        '''
        self.set_notation(notations)
        initial_expression = step_hard_data
        initial_pattern = milestone_hard_data
        
        #преобразуем данные к виду, который понимает SymPy
        expression = self.make_readable_for_sympy(initial_expression)
        pattern = self.make_readable_for_sympy(initial_pattern)
        #разбиваем выражения на фрагменты между знаками =
        pattern = pattern.split('=')
        expression = expression.split('=')
        #количество знаков равенства введённх студентоом не совпадает с тем, что заложено в системе
        #todo: нужна ли в этом случае более хитрая проверка?
        if(len(pattern) != len(expression)):
            return 0
        #нет равенств
        if(len(pattern) == 1):
            return self.check_expression(pattern[0], expression[0], check_type)
        #одно равенство a1=b1 и a2=b2, в этом случае сравниваем (a1==a2&&b1==b2)||(a1==b2&&b1==a2)
        #todo: такая проверка не подходит для решения уравнений
        elif(len(pattern) == 2):
            if(self.check_expression(pattern[0], expression[0], check_type) and self.check_expression(pattern[1], expression[1], check_type)):
                return 1
            if(self.check_expression(pattern[0], expression[1], check_type) and self.check_expression(pattern[1], expression[0], check_type)):
                return 1
            # для выражений типа |c|=\sqrt{(c,c)} и (|c|)^2=(c,c) проверка выше не подходит
            expression_1 = self.remove_sqrt(initial_expression)
            pattern_1 = initial_pattern
            expression_1 = self.make_readable_for_sympy(expression_1)
            pattern_1 = self.make_readable_for_sympy(pattern_1)
            expression_1 = expression_1.split('=')
            pattern_1 = pattern_1.split('=')
            if ((self.check_expression(expression_1[0] + '- (' + expression_1[1] + ') - (' + pattern_1[0] + ') + '+ pattern_1[1], '0', check_type)) or (self.check_expression(expression_1[0] + '- (' +  expression_1[1] + ') + '+ pattern_1[0]  + '- (' + pattern_1[1]+ ')', '0', check_type))):
                return 1
            expression_2 = initial_expression
            pattern_2 = self.remove_sqrt(initial_pattern)
            expression_2 = self.make_readable_for_sympy(expression_2)
            pattern_2 = self.make_readable_for_sympy(pattern_2)
            expression_2 = expression_2.split('=')
            pattern_2 = pattern_2.split('=')
            if ((self.check_expression(expression_2[0] + '- (' + expression_2[1] + ') - (' + pattern_2[0] + ') + '+ pattern_2[1], '0', check_type)) or (self.check_expression(expression_2[0] + '- (' +  expression_2[1] + ') + '+ pattern_2[0]  + '- (' + pattern_2[1]+ ')', '0', check_type))):
                return 1
            
        return 0
