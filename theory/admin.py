from django.contrib import admin
from theory.models import Theory, Theory_section
from django import forms
from theory.widgets import CKEditorWidget


class TheoryAdminForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Theory
        
class TheoryAdmin(admin.ModelAdmin):
    model = Theory
    form = TheoryAdminForm

admin.site.register(Theory, TheoryAdmin)
admin.site.register(Theory_section)