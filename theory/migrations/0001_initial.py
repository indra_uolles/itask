# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Theory_section'
        db.create_table('theory_theory_section', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=300, blank=True)),
        ))
        db.send_create_signal('theory', ['Theory_section'])

        # Adding model 'Theory'
        db.create_table('theory_theory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('section', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['theory.Theory_section'], null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('theory', ['Theory'])

        # Adding M2M table for field tasks on 'Theory'
        db.create_table('theory_theory_tasks', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('theory', models.ForeignKey(orm['theory.theory'], null=False)),
            ('task', models.ForeignKey(orm['tasks.task'], null=False))
        ))
        db.create_unique('theory_theory_tasks', ['theory_id', 'task_id'])


    def backwards(self, orm):
        # Deleting model 'Theory_section'
        db.delete_table('theory_theory_section')

        # Deleting model 'Theory'
        db.delete_table('theory_theory')

        # Removing M2M table for field tasks on 'Theory'
        db.delete_table('theory_theory_tasks')


    models = {
        'tasks.task': {
            'Meta': {'ordering': "['name']", 'object_name': 'Task'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'theory.theory': {
            'Meta': {'ordering': "['title']", 'object_name': 'Theory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'section': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['theory.Theory_section']", 'null': 'True', 'blank': 'True'}),
            'tasks': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['tasks.Task']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        'theory.theory_section': {
            'Meta': {'ordering': "['name']", 'object_name': 'Theory_section'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'})
        }
    }

    complete_apps = ['theory']