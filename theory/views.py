from django.shortcuts import render_to_response
from django.http import Http404
from theory.models import Theory, Theory_section
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from students.models import Student, Session, Attempt, Step, Log, Saving
from datetime import datetime

@login_required
def show_theory(request, theory_id):
    
    student = Student.objects.get(user=request.user)
    
    if not 'student_session_id' in request.session:
        session = Session(identificator=str(datetime.now()) + ' ' + str(student.id), student=student)
        session.save()
        request.session['student_session_id'] = session.id
        
    else:
        try:
            session = Session.objects.get(id=int(request.session['student_session_id']))
        except Session.DoesNotExist:
            session = None
    
    request.session.set_expiry(8640)
    
    try:
        if theory_id != 0:
            text = Theory.objects.get(id=theory_id)
        else:
            text = ''
        contents = Theory_section.objects.select_related().all()
    except Theory.DoesNotExist:
        return Http404

    return render_to_response("theory.html", \
    				{'text': text, 'contents': contents}, \
    				context_instance=RequestContext(request))