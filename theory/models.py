# coding: utf-8
from django.db import models
from tasks.models import Task

# Create your models here.
class Theory_section(models.Model):
    name = models.CharField(blank=True, max_length=300, verbose_name='Раздел теории')
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ['name']

class Theory(models.Model):
    section = models.ForeignKey(Theory_section, blank=True, null=True, verbose_name='Раздел теории')
    tasks = models.ManyToManyField(Task, blank=True, null=True, verbose_name='Список задач')
    title = models.CharField(max_length=300, verbose_name='Название')
    text = models.TextField(verbose_name='Содержание')

    def __unicode__(self):
        return self.title
    
    class Meta:
        ordering = ['title']
