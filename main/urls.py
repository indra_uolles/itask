from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    # ===================== Tasks ====================================
    url(r'^$', 'theory.views.show_theory', {'theory_id': 0}, name='show_theory'),
    url(r'^tasks/(?P<task_id>\d+)$', 'tasks.views.solve_task', name='solve_task'),
    # Urls for debugging
    url(r'^show_all_hints$', 'tasks.views.show_all_hints', name='debug_show_all_hints'),
    url(r'^show_filtered_hints$', 'tasks.views.show_filtered_hints', name='show_filtered_hints'),
    url(r'^show_all_solutions$', 'tasks.views.show_all_solutions', name='debug_show_all_solutions'),
    url(r'^show_filtered_solutions$', 'tasks.views.show_filtered_solutions', name='show_filtered_solutions'),
    url(r'^show_all_complaints$', 'tasks.views.show_all_complaints', name='show_all_complaints'),

    # ===================== Tasks - Solving ====================================
    url(r'^get_grade$', 'tasks.views.get_grade', name='get_grade'),
    url(r'^get_milestone_id$', 'tasks.views.get_milestone_id', name='get_milestone_id'),
    url(r'^get_filtered_hint$', 'tasks.views.get_filtered_hint', name='get_filtered_hint'),
    url(r'^get_plan_hint$', 'tasks.views.get_plan_hint', name='get_plan_hint'),
    url(r'^send_step$', 'tasks.views.check_step', name='send_step'),
    url(r'^save_step_easy_interface$', 'tasks.views.save_step_easy_interface', name='save_step_easy_interface'),
    url(r'^remove_step/(?P<step_id>\d+)$', 'tasks.views.remove_step', name='remove_step'),
    url(r'^update_solution/(?P<step_id>\d+)$', 'tasks.views.update_solution', name='update_solution'),
    url(r'^modify_step/(?P<step_id>\d+)$', 'tasks.views.modify_step', name='modify_step'),
    url(r'^get_easy_template/(?P<milestone_id>\d+)$', 'tasks.views.get_easy_template', name='get_easy_template'),
    url(r'^get_easy_data_check_data/(?P<milestone_id>\d+)$', 'tasks.views.get_easy_data_check_data', name='get_easy_data_check_data'),
    url(r'^get_easy_data_depend_on/(?P<milestone_id>\d+)$', 'tasks.views.get_easy_data_depend_on', name='get_easy_data_depend_on'),
    url(r'^complain$', 'tasks.views.complain', name='complain'),
    
    # ======================Logs========================================================================================================== 
    url(r'^write_log$', 'tasks.views.write_log', name='write_log'),
    url(r'^remove_logs$', 'students.views.remove_logs', name='remove_logs'),
    url(r'^logs$', 'students.views.all_logs', name='all_logs'),
    url(r'^show_logs/(?P<session_id>\d+)$', 'students.views.show_logs', name='show_logs'),

    # ===================== Students =====================================================================================================
    url(r'^attempts$', 'students.views.all_attempts', name='all_attempts'),
    url(r'^session_attempts/(?P<session_id>\d+)$', 'students.views.session_attempts', name='session_attempts'),
    url(r'^student/(?P<student_id>\d+)$', 'students.views.student_attempts', name='student_attempts'),
    url(r'^session/(?P<session_id>\d+)$', 'students.views.session_attempts', name='session_attempts'),

    # ===================== Students - Attempts ====================================
    url(r'^attempt/(?P<attempt_id>\d+)$', 'students.views.show_attempt', name='show_attempt'),
    url(r'^remove_attempts$', 'students.views.remove_attempt', name='remove_attempts'),
    url(r'^save$', 'students.views.save_attempt', name='save_attempt'),
    url(r'^load/(?P<saving_id>\d+)$', 'tasks.views.load_saving', name='load_attempt'),
    url(r'^hint_attempt/(?P<attempt_id>\d+)', 'students.views.hint_attempt', name='hint_attempt'),
    url(r'^close_attempt/(?P<grade>\d+)$', 'students.views.close_attempt', name='close_attempt'),
    url(r'^revive_attempt/(?P<attempt_id>\d+)$', 'students.views.revive_attempt', name='revive_attempt'),
    url(r'^set_active_attempt/(?P<attempt_id>\d+)$', 'tasks.views.set_active_attempt', name='set_active_attempt'),

    # ===================== Students - Authentication ====================================
    url(r'students/login/$', 'django.contrib.auth.views.login',
                             {'template_name': 'login.html'}, name='login'),
    url(r'students/logout/$', 'django.contrib.auth.views.logout_then_login',
                              {'login_url': '/students/login/'}, name='logout'),

    # ===================== Theory ====================================
    url(r'^theory/(?P<theory_id>\d+)$', 'theory.views.show_theory', name='show_theory'),
    url(r'^theory/$', 'theory.views.show_theory', {'theory_id': 0}, name='show_theory'),

    # ===================== Admin ====================================
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    #=======================Tutoring flow Control ====================
    url(r'^suggest_help$', 'tasks.tutoring_flow_control.suggest_help', name='suggest_help'),
    url(r'^get_filtered_hint$', 'tasks.views.get_filtered_hint', name='get_filtered_hint'),

    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
)
