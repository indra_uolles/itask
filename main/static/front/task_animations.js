
/**
  * Анимация, показываемая во время проверки введенного шага
  * @INFO: сделал динамически формируемый <div> чтобы (а) не захламлять код
  * шаблона и (б) размеры div могут меняться при переключении на разные 
  * типы интерфейса.
  */
function Check_progress_animation() {
    this.show = function() {
        var $student_step = $('#step-template .student-step');
        var loader_html = '<div class="check_progress_animation student-step well" style="text-align:center"><div class="alert alert-block" style="margin-top:60px"><b>Проверка шага...</b><br><img src="/static/img/ajax-loader.gif"></div></div>'
        var $loader = $(loader_html).css({
            position: 'absolute',
            'z-index': 10,
            top: $student_step.offset().top,
            left: $student_step.offset().left,
            width: $student_step.width(),
            height: $student_step.height(),
            background: 'rgba(0, 0, 0, 0.5)',
        }).hide();
        $('body').append($loader);
        $loader.fadeIn(200);
    }

    this.hide = function() {
        $('.check_progress_animation')
            .fadeOut(200, function(){$(this).remove()});
    }
}



/**
  * Анимация, появляющаяся при правильно введеном шаге.
  * На вход принимает html-текст описания правильно введеного шага.
 **/
function animation_of_adding_correct_solution(solution_entry_text) {
    var $solution_entry = $('<div class="solution_entry">' + solution_entry_text + '</div>');
    var $solution_container = $('#solution');
    
    // 1. Зададим откуда начнет движение блок с принятым решением
    var start_pos = $('.student-step').offset();
    start_pos.top = start_pos.top + 15;
    $solution_entry.css({
        position: 'absolute',
        top: start_pos.top + 'px',
        left: (start_pos.left + 20) + 'px',
        'z-index': 10000,
        opacity: 0
    });
    $('body').prepend($solution_entry);


    // 2. Вставим заглушку, чтобы решение прилетало на спец. выделенное место
    var $dummy = $('<div>').css('height', $solution_entry.height());
    $solution_container.append($dummy);
    
    // 3. Скроллим до последнего элемента
    var scrollToPosition = $solution_container.height() - $solution_container.parent().height();
    $solution_container.parent()
        .animate({scrollTop: scrollToPosition}, 200,
         function() {
            // Выполняется по окончанию анимации скролла
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, $solution_entry.get()]);

            // 4. Двигаем принятое решение на место заглушки
            topMoveDistance = start_pos.top - $dummy.offset().top;
            $solution_entry.animate({
                    opacity: 1,
                    top: '-=' + topMoveDistance + 'px'
                }, 1000,
                function() {
                    // 5. После окончания анимации перемещения удаляем заглушку
                    // добавляем solution_entry в общий список в #solution
                    $dummy.remove();
                    $solution_container.append($(this));
                    $(this).css('position', 'static');
                    MathJax.Hub.Queue(["Typeset", MathJax.Hub, "solution"]);         
                });
         });

}