//для управления уч. - когда была выдана последняя конкретная подсказка
var lt_con_hint;

//для управления уч. - когда была выдана последняя любая подсказка
var lt_any_hint;

//для управления уч. - сколько было выдано подсказок за последнюю мин
var ft_hq = 0;
//переменная для управления ограничения подсказками
var set = true;

function show_hints_window(){
	//если открыто окно обозначений, нужно его закрыть
	if ($(".pop-out-notations").hasClass("hide") == false){
		$(".pop-out-notations").slideToggle("medium");
		$(".pop-out-notations").addClass("hide");
	}
	$(".pop-out-hints").slideToggle("medium");
	$(".pop-out-hints").toggleClass("hide");
}

//функция получения подсказки по ходу решения
function get_plan_hint(initiator, step){
	var step_number = step.find('.step-number').html(); 
	if (initiator == 'student'){
		logger.add_to_log('STUDENT ASK_HELP PLAN_HINT ');
		//упр. уч. процессом
	    task_id = parseInt($('#task_id').html());
	    if (task_id !=5 && task_id !=6){
        time_now = new Date().getTime();
        if (ft_hq == 4){
        	modalMessageAttention('В помощи отказано, поскольку нельзя получать более 4 подсказок меньше чем за 1 минуту. Попробуйте пока подумать сами.');                
            logger.add_to_log('SYSTEM REFUSE HELP EXCEED_MINUTE_HINT_LIMIT ');
            if (set == true){
            	window.setInterval(reset_hint_control_params, 60000);
            	set = false;
            }            
            return false;       
        }
	    }
		
		$("#loading_hint").removeClass("hide");
	    $.get('/get_plan_hint', {'step_number': step_number, 'possible_milestones': possible_milestones.join(','), 'combinatorial_milestones': combinatorial_milestones.join(',')}, function(data){
	    	$("#loading_hint").addClass("hide");

	        if (data == 'finish') {
	            modalMessageAttention('По всей видимости, вы уже ввели все шаги');	            
	            logger.add_to_log('SYSTEM REFUSE HELP NO_HELP');
	            return;
	        }
	        else if (data == ''){
	            modalMessageAttention('К сожалению, пока не получается понять ваше решение. Поэтому невозможно предоставить подсказку по ходу решения');
	            logger.add_to_log('SYSTEM REFUSE HELP UNKNOWN_SOLUTION');
	            return;
	        }
	        else{
	        	show_hint(data, 4, step_number, 'student');	        	
	        }

	    });
	}
	else{
		logger.add_to_log('STUDENT AGREE_HELP PLAN_HINT ');
		$.get('/get_plan_hint', {'step_number':step_number, 'possible_milestones': possible_milestones.join(','), 'combinatorial_milestones': combinatorial_milestones.join(',')}, function(data){
			$("#loading_hint").addClass("hide");
	        show_hint(data, 4, step_number, 'system', 0);
		});		
	}
}

function get_time_difference_minutes(earlierDate,laterDate)

{
       var nTotalDiff = laterDate - earlierDate;
       
       var daysDifference = Math.floor(nTotalDiff/1000/60/60/24);
       nTotalDiff -= daysDifference*1000*60*60*24
    
       var hoursDifference = Math.floor(nTotalDiff/1000/60/60);
       nTotalDiff -= hoursDifference*1000*60*60
    
       var minutesDifference = Math.floor(nTotalDiff/1000/60);
       nTotalDiff -= minutesDifference*1000*60
 
       //var secondsDifference = Math.floor(nTotalDiff/1000);       
       return minutesDifference 
}


/**
  * Функция, обновляющая вид поля "Полученные подсказки"
  **/ 
function show_hint(data, type_hint, step_number, initiator, milestone_id){
	var hint_kind = 0;
	milestone_id = milestone_id || 0; 
	data = light_simple_to_mathjax(data);
    num_hints += 1;
    $('#hints').append('<div><a name="hint'+num_hints+'"></a>'+data+'</div><br />'); 
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, "hints"]);
    var id_hint = '';

    $('#hints').animate({scrollTop: $('#hints')[0].scrollHeight});

    if (type_hint != 4){
	    // берется id загруженной подсказки и добавляется к id подсказкам 
	    id_hint = data.split("<!--")[1].split("-->")[0];
	    hints_ids[milestone_id].push(id_hint);
	    hint_name = 'hint'+num_hints;
	    hint_kind = parseInt($('a[name=hint_name]').find('.hint_kind').html());
    }

    if ($(".pop-out-hints").hasClass("hide") == true){
        show_hints_window(); 
    }
    
    str = '';
    if (type_hint == 1){
    	str = 'STUDENT RECEIVE_HELP THEORETIC ';
    }
    else if (type_hint == 2){
    	str = 'STUDENT RECEIVE_HELP SCHEMATIC ';
    }
    else if (type_hint == 3){
    	str = 'STUDENT RECEIVE_HELP CONCRETE ';
    	var hint_text = "";
    	if (hint_kind != 3){
    	hint_text = data.split("<p>")[1].split("</p>")[0];
    	}
    	else{
    		hint_text = "CHANGE_TASK"
    	}
    }
    else{
    	str = 'STUDENT RECEIVE_HELP PLAN_HINT ';
    }
    task_id = parseInt($('#task_id').html());
    var hint_text = "";
    if (hint_kind != 3){
    	hint_text = data.split("<p>")[1].split("</p>")[0];
     	logger.add_to_log('STUDENT RECEIVE_HINT ' + task_id + ' ' + hint_text);
    }	
    else{
    	logger.add_to_log('STUDENT RECEIVE_HINT ' + task_id + ' CHANGE_TASK');
    }
    if (hint_kind == 2){
		str = str + 'THEORY ';
    }
    else if (hint_kind == 3){
    	str = str + 'CHANGE_TASK ';
    }
    str = str + id_hint;
    logger.add_to_log(str);
    //для управления уч. процессом
    //учитываем только подсказки, полученные по запросу студента
    if (initiator == 'student'){
    	change_hint_control_params(type_hint);
    }
}   

function change_hint_control_params(type_hint){
	time_now = new Date().getTime();
	if (type_hint == 3){		
    	lt_con_hint = time_now;
    }
    //если разница между текущей и предыдущей подсказкой < 5 мин, то 
    //прибавляем счетчик, иначе считаем заново
    min_ago = 0;
	if (lt_any_hint === undefined){
		min_ago = -1;
	}
	else{
		min_ago = get_time_difference_minutes(lt_any_hint, time_now);				
	}
    if (min_ago >= 1){
    	ft_hq = 1;
    }
    else{
    	ft_hq = ft_hq + 1;
    }
    lt_any_hint = time_now;
}

function reset_hint_control_params(){
	ft_hq = 0;
	set = true;
}

//функция получения подказки по шагу
function get_hint(initiator, step, type_hint){
    var hint_types = ['THEORETIC', 'SCHEMATIC', 'CONCRETE'];
	//значение по умолчанию
	type_hint = type_hint || 0;    
    milestone_id = step.attr('milestone-id');
    hint_filter = 0;
    
    var easy = 0;
    if (step.find(".toggle_interface").hasClass("easy")){
    	easy = 1;
    }  
    
    var step_number = step.find('.step-number').html(); 
    
	if (initiator == 'student'){
        logger.add_to_log('STUDENT ASK_HELP ' + hint_types[type_hint-1]);
        
       task_id = parseInt($('#task_id').html());
       if (task_id !=5 && task_id !=6){
        //упр. уч. процессом - работает во всех задачах кроме задач по вводу данных
        var time_now = new Date().getTime();
       if (ft_hq == 4){
        	modalMessageAttention('В помощи отказано, поскольку нельзя получать более 4 подсказок меньше чем за 1 минуту. Попробуйте пока подумать сами.');                
            logger.add_to_log('SYSTEM REFUSE HELP EXCEED_MINUTE_HINT_LIMIT ');
            if (set == true){
            window.setInterval(reset_hint_control_params, 60000);
            set = false;
            }
            return false;       
        }
        if (type_hint==3){
        	if (!(lt_con_hint === undefined)){
					time_now = new Date().getTime();
					min_ago = get_time_difference_minutes(lt_con_hint, time_now);				
        			if (min_ago <= 2){
        				modalMessageAttention('В помощи отказано, поскольку нельзя получать более 2 конкретных подсказок меньше чем за 2 минуты. Попробуйте пока подумать сами.');                
            			logger.add_to_log('SYSTEM REFUSE HELP EXCEED_CONCRETE_HINT_LIMIT');
            			return false;    
       				}       		
        	}
        }
       }
               
        if (milestone_id == undefined || milestone_id == 0) {
            if (easy == 1){
                modalMessageAttention('Помощь по шагу можно получить только после того, как укажете тип \
                шага. Пока вы можете только получить подсказку по общему ходу решения и – в любой момент – \
                    помощь по вводу данных');                
                logger.add_to_log('SYSTEM REFUSE HELP NO_MILESTONE ');
                return false;
            } else {
            	milestone_id = get_auto_milestone_id();
            	if (milestone_id == '-1'){
            		modalMessageAttention('По-видимому, вы уже ввели все шаги');
            		logger.add_to_log('SYSTEM REFUSE HELP NO_HELP ');
            		return false;            		
            	}
            	else if (milestone_id == '-2'){
            		modalMessageAttention('К сожалению, не получилось понять ваше решение. Ваша попытка решения задачи будет оценена преподавателем');           		
            		logger.add_to_log('SYSTEM REFUSE HELP UNKNOWN_SOLUTION ');
            		return false;
            	}            	
            }
        }
	} else {
        switch (selected_help) {
            case 'theoretic':
                type_hint = 1;
                hint_filter = 1;
                break;
            case 'schematic':
                type_hint = 2;
                hint_filter = 1;
                break;
            case 'concrete':
                type_hint = 3;
                hint_filter = 1;
                break;
            case 'theory':
                type_hint = 1;
                hint_filter = 2;
                break;
            case 'change_task':
                type_hint = 3;
                hint_filter = 3;
                break;
        }

        logger.add_to_log('STUDENT AGREE_HELP ' + hint_types[type_hint-1]);

        milestone_id = get_auto_milestone_id();           
	}
	
    if ( typeof hints_ids[milestone_id] == 'undefined' )
        hints_ids[milestone_id] = Array();

    $("#loading_hint").removeClass("hide");
    
    $.get('/get_filtered_hint',
         {'milestone_id': milestone_id,
          'hint_type': type_hint,
          'easy': easy,
          'hints_ids': hints_ids[milestone_id].join(","),
          'hint_filter': hint_filter,
          'step_number': step_number},
     function(data){
    	$("#loading_hint").addClass("hide");
    	if (data =='' && initiator == 'student') {
            modalMessageAttention('Подсказок нужного вам типа для данного шага не осталось.');
            $("#loading_hint").addClass("hide");
            logger.add_to_log('SYSTEM REFUSE HELP NO_HELP ');
            return;                
        } else {
            show_hint(data, type_hint, step_number, initiator, milestone_id);
        }
    })	
}   

//автоматически определяет тип шага, над которым сейчас работает студент
function get_auto_milestone_id() {
	milestone_id = 0;	
    $.ajax({
    	url: '/get_milestone_id',
    	async: false,
    	type: 'GET',
    	data: {'possible_milestones': possible_milestones.join(","), 'combinatorial_milestones': combinatorial_milestones.join(",")},
    }).done(function(data) {
    	data = $.parseJSON(data);
		milestone_id = data.milestone_id;
    });
	return milestone_id
}

function modalMessageAttention(text) {
	   $("#modal_message_attention").find(".modal-body > p").html(text);
	   $("#modal_message_attention").modal();
};