    //это для проверки шага с помощью скрипта, возможно, надо будет переделать
/*    $('#notebook-workspace').on('focusout', '.easy-interface-explanation', function(){
    	
    	//узнаем, что за шаг 
    	step = $(this).closest(".step-container");
    	//извлекаем введенное студентом значение
    	var data = $(this).val();
    	var id = $(this).attr('id');
    	//обновляем данные о введенных студентом значениях в легкий интерфейс шага
    	update_send_data(step, id, data);
    	return false;
    	
    });
    
    $('#notebook-workspace').on('click', 'input[type=checkbox]', function(){
        if ($(this).hasClass('easy-interface-explanation'))
        {
        	//извлекаем введенное студентом значение
        	if ($(this).is(':checked')){
        		data='1';
        	}
        	else{
        		data='0';
        	}
        	//узнаем, что за шаг 
        	step = $(this).closest(".step-container");
        	//обновляем данные о введенных студентом значениях в легкий интерфейс шага
        	var id = $(this).attr('id');       	
        	update_send_data(step, id, data);
        }

    });*/

$('#solution').mousemove(function(e){
	Do('solution_mouse_move', 'STUDENT MOVE_MOUSE SOLUTION ', e);
});
	
//сколько по времени студент скроллил solution
$('#solution').scroll(function(e){	
	Do('solution_scroll', 'STUDENT SCROLL SOLUTION ', e);
});

function doStop(selector, action_message){
	arr = get_arr(selector);
	arr['isDoing'] = false;
	arr['secondsDoing'] = ((new Date).getTime() - arr['secondsDoing'])/1000;
	logger.add_to_log(action_message + arr['secondsDoing']);
	console.log(action_message + arr['secondsDoing']);
	update_arr(selector, arr);
}

function Do(selector, action_message, e){
	arr = get_arr(selector);
	clearTimeout(arr['timer']);
	if (!arr['isDoing']) {
		arr['isDoing'] = true;
		arr['secondsDoing'] = (new Date).getTime();
	}
	update_arr(selector, arr);
	timer = setTimeout(doStop(selector, action_message), 1000);
	arr['timer'] = timer;
	update_arr(selector, arr);
    e.preventDefault();
    e.stopPropagation();
}

function add_arr(selector){
	arr = {};
	arr['timer']=null;
	arr['isDoing']=false;
	arr['secondsDoing']=0;
	do_arrs[selector]= arr;	
}

function get_arr(selector){
	if (do_arrs[selector] === undefined){
		add_arr(selector);
	}
	return do_arrs[selector];
}

function update_arr(selector, arr){
	do_arrs[selector] = arr;
}