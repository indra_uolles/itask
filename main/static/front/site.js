var step;
var hints_ids = {};
//расшифровки ситуаций см в модуле управления учебным процессом
var situations = [0, 0, 0];
// количество подсказок
var num_hints = 0;
var page_url = location.href.split('#')[0];
var scroll_timeout;
var warning_easy_interface = true;
var step_type_enabled = false;
var try_again = false;
//оценка за попытку
var grade = 0;
//введена потому что дублируются события window on focus
var window_focus = false;
var attempt_id;
var help_enabled;
//ожидание ответа студента - флаг для следующего вызова модуля управления учебным процессом
var waiting=false;
//алгоритм управления должен запускаться только если на экране текущее окно - попытка решения задачи
var active = true;
//помощь, выбранная для студента модулем управления учебным процессом на основе плановой проверки действий студента
var selected_help;
//чтобы исключить события, вызванные программно, а не студентом из лога
var auto_scrolling = false;
var logger;
//для сохранения сведений о том, какие этапы могут соответствовать корректному шагу студента
var possible_milestones = new Array();
//для сохранения сведений о том, какие комбинаторные этапы реализованы в решении студента
var combinatorial_milestones = new Array();
//предупреждение для механизма жалоб
warning_complain = true;

$(function(){

// ================= Initialization =========================================
$('[rel="tooltip"]').tooltip({'placement': 'bottom'});
step = $('#notebook-workspace').find('.step-container');
$('.step-input').focus(); 

MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [['$','$'], ['\\(','\\)']],
        displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
        processEscapes: true
    },
     jax: ["input/TeX", "output/HTML-CSS"],
     "HTML-CSS": {
     availableFonts: ["TeX"],
     preferredFont: "TeX",
     webFont: "TeX",
     imageFont: null }
});
//=============== END Configuration Mathjax================================================================================================

 // После ввода формулы – обновить математическое представление введенного текста
(function(){
	var timer = null;
	var isMouseMoving = false;
	var secondsMoving = 0;

	function mouseStop() { 
		isMouseMoving = false
		secondsMoving = ((new Date).getTime() - secondsMoving)/1000
		var step_number = step.find('.step-number').html(); 
		logger.add_to_log('STUDENT EDIT STEP ' + secondsMoving);
	}
	
 $('#notebook-workspace').on('keyup', '.step-input', function(e){
     var input = simple_to_mathjax($(this).val());

     $input = $(this).parentsUntil('.step-container').find(".inputted-formula");
     $input.text('$$ ' + input + ' $$');
     $(this).attr("data", $(this).val());

     MathJax.Hub.Queue(["Typeset", MathJax.Hub, $input.get()[0]]);	     
	
	clearTimeout(timer)
	timer = setTimeout(mouseStop, 1000)
	if (!isMouseMoving) {
		isMouseMoving = true
		secondsMoving = (new Date).getTime()
	}
    });
    
})();

$('#notebook-workspace').on('focus', '.step-input', function(e){
	// если до этого шаг был проверен и оказался неправильным, нужно снять красную окраску и изменить заголовок на нейтральные
	if (!$(this).val() == '') {			
		remove_wrong_view(step);			
	}	
});

(function(){
	var timer = null;
	var isMouseMoving = false;
	var secondsMoving = 0;

	function mouseStop() { 
		isMouseMoving = false
		secondsMoving = ((new Date).getTime() - secondsMoving)/1000
		var step_number = step.find('.step-number').html(); 
		logger.add_to_log('STUDENT PRESS_DELETE_OR_BACKSPACE INPUT' + secondsMoving);
	}

$('#notebook-workspace').on('keydown', '.step-input', function(e){
	var keyID = e.keyCode;	
	if (keyID == 8 || keyID == 46){
		clearTimeout(timer)
		timer = setTimeout(mouseStop, 1000)
		if (!isMouseMoving) {
			isMouseMoving = true
			secondsMoving = (new Date).getTime()
		}
	}
   });  
})();
    
var task_id = parseInt($('#task_id').html());
var attempt_id = parseInt($('#attempt_id').html());

// разрешены ли подсказки
if (help_enabled == 1){
	reset_hints_no_log();
	setTimeout(reset_hints_no_log, 1000);
    $(".show_hints_window").addClass('hide');
    $("#get_plan_hint").addClass('hide');
    $(".step-hint").addClass('hide');
}

logger = new Logger();
logger.add_to_log('STUDENT START_ATTEMPT ' + attempt_id + task_id);
    
//=======================Операции с шагами, не зависящие от интерфейса (добавить, выбрать тип, удалить, проверить)==========================
   
//механизм жалоб
//окно выбора типа шага
$('#notebook-workspace').on('click', '.complain', function(){
	var step_number = step.find('.step-number').html(); 	
	logger.add_to_log('STUDENT TRY COMPLAIN ' + step_number);
	
	if (step.find('.toggle_interface').hasClass('easy')){
		if (def_all_filled(step) == false) {
			modalMessageAttention('Заполнены не все ячейки, шаг заведомо неправильный, поэтому пожаловаться на него нельзя');
			logger.add_to_log('SYSTEM REFUSE COMPLAIN NOT_ALL_FILLED');
			return false;
		}
	}	
	
	//данная функция срабатывает только если статус шага сейчас неправильный
	if (step.find('.control-group').hasClass('error')){
		
	    if (warning_complain == true) {
	        warning_complain = false;
	        	        
	        modalMessageYesNo('Если вы пожалуетесь, то ваш текущий шаг будет оцениваться не системой,  а преподавателем. Будете жаловаться?', 'complain');

	        return false;
	    }
	    //студент жалуется, студента уже предупреждали
	    else{
	    	logger.add_to_log('STUDENT COMPLAIN ');
	    }
		complain();  			
	}
	else{
		modalMessageAttention('Текущий шаг не имеет статуса неправильного, поэтому пожаловаться на него нельзя');
		logger.add_to_log('SYSTEM REFUSE COMPLAIN STEP_NOT_WRONG');
	}
    return false;
});

function complain(){
	str = "";
	//добавляем данные шага в поле "Решение"
	var step_number = step.find('.step-number').html(); 
	if (step.find('.toggle_interface').hasClass('easy')){
        str = get_formula(step);		
	}
	else{
		str = step.find('.step-input').val();
	}
	str = light_simple_to_mathjax(str);
	var solution_entry = '<b>Шаг ' + step_number + '</b> <br/><a name="step' + parseInt(step_number) + '"></a>' + str + '<br/><br/>';
    // Анимация правильно введенного шага (функция в task_animations.js)
    animation_of_adding_correct_solution(solution_entry);
    $('#solution').animate({scrollTop: $('#solution')[0].scrollHeight});
    $('#solution-content').append(solution_entry);
    $('#solution-content').animate({scrollTop: $('#solution-content')[0].scrollHeight});
	clear_step_view(step);
    //последнему добавленному шагу в базу данных студента добавляем статус "в прогрессе"
    //а в форме попытки все шаги с таким статусом помечаем цветом
	$.ajax({
		url: '/complain',
		async: false			
	}).done(function(data){
	});
}

// окно выбора типа шага
$('#notebook-workspace').on('click', '.select-step-type', function(){
	logger.add_to_log('STUDENT OPEN SELECT_STEP ');
    $("#modal_select_step").modal();
    return false;
});

$('#modal_select_step').on('hidden', function () {
	close_reason = parseInt($("#modal_select_step").find('.close-reason').html());
	if (close_reason == 0){
		logger.add_to_log('STUDENT CLOSE SELECT_STEP ');
	}
	else if (close_reason == 2){
		logger.add_to_log('STUDENT CANCEL SELECT_STEP ');
	}
	$("#modal_select_step").find('.close-reason').html('0');
})

// отмена выбора шага
$('#notebook-workspace').on('click', '#btn-cancel-select-step-type', function(){
	//причина закрытия - 0 - просто закрыли, 1 - выбрано значение, 2 - отмена
	$("#modal_select_step").find('.close-reason').html('2');
    $("#modal_select_step").modal('hide');
    return false;
});

// выбор типа шага
$('#notebook-workspace').on('click', '#btn-select-step-type', function(){
	remove_wrong_view(step);

    // идентификатор типа шага
    var milestone_id = $('.milestone_type').val();
    
    //для логирования
    $("#modal_select_step").find('.close-reason').html('1');
    
    var step_number = step.find('.step-number').html(); 
    
	logger.add_to_log('STUDENT SELECT_MILESTONE ' + milestone_id);
    
    // название типа шага
    var milestone_val = $('.milestone_type > option[value="'+milestone_id+'"]').html();
    
    // помещаем название в блок вводимого шага
    step.find(".title_step").html(', тип: ' + milestone_val);
    // помещаем идентификатор типа шага в блок шага
    step.attr('milestone-id', milestone_id);

    // подгружаем легкий интерфейс
    load_easy_interface(step, milestone_id);       

    // убираем модальное окно выбора шага
    $("#modal_select_step").modal('hide');
    
    return false;        
});
          
// Проверка шага
$('#notebook-workspace').on('click', '.check-step', function() {
    // Включим анимацию проверки шага (класс в task_animations.js)
    var check_animation = new Check_progress_animation();
    check_animation.show();


    var step_number = step.find('.step-number').html(); 
    if (!step.find(".toggle_interface").hasClass("easy")) {
    		check_step_formula(step, task_id);
    } else {
    	if (step.find('.check-type').html() == '') {
        	logger.add_to_log('STUDENT CHECK_STEP WRONG EMPTY_DATA ' + step_number);
            change_step_status(step, 0);
       		modalMessageAttention('Дозаполните данные шага');
    	}
        var check_type = parseInt(step.find('.check-type').html());
    	
        //тип проверки - формула
        if (check_type == 1) {
        
	        //если не все значения заполнены - отказываемся от проверки
	        if (step.find(".toggle_interface").hasClass("easy")) {
		        	var send_data = step.find('.send-data').html();
		            //вспомогательная переменная - все ли введены значения
		        	var variables = step.find('.variables').html();
		        	variablesArray = variables.split(';')
		            var all_filled = 1;
		            for (var i = 0; i < variablesArray.length; i++) {
		            	//для каждой переменной находим ее значение, введенное студентом
		            	var variableName = variablesArray[i]; 
		            	var expression = '(\\[' + variableName + '\\]=)(.*?)(;)';
		            	var myRegexp = new RegExp(expression, 'g');
		            	var match = myRegexp.exec(send_data);
		            	//если студент не ввел какое-то значение, то шаг неверен
		            	if (match == null) { 
		            		all_filled = 0;
		            	} 
		            }
		            	if (all_filled == 0){
		            		logger.add_to_log('STUDENT CHECK_STEP WRONG PARTIALLY_FILLED_DATA ' + step_number);
		            		modalMessageAttention('Дозаполните данные шага');
		            	}  
		            	else{
		            		check_step_formula(step, task_id);
		            	}
	        }
	        else{
	        	check_step_formula(step, task_id);
	        }
        	
        }        	
        //тип проверки - конкретные значения или алгоритм, у шага легкий интерфейс
        else if (check_type == 2) {
        	check_step_values(step);   		    
        }
        
        //тип проверки - скрипт, у шага легкий интерфейс
        else {
        	//проверку шага с помощью скрипта надо будет переделать, пока она просто не поддерживается
        	//check_step_script(step);	
        }
    }

    //Завершим анимацию проверки шага (класс в task_animations.js)
    setTimeout(function(){check_animation.hide();}, 1000)
    return false;
});    
   
//=========================================================================================================================================
       
//Функции нужные и в легком, и в трудном интерфейсе========================================================================================
function change_interface(){

    step.find('.toggle_interface').toggleClass('easy');
    step.find('.input-rules').toggleClass('hide');
    if (step.find('.toggle_interface').hasClass('easy')){
        // переключение на легкий интерфейс

        // показываем кнопку сброса и панель ввода формулы
        step.find(".reset-easy").removeClass("hide");
        step.find('.easy-inputted-formula').removeClass('hide');
        
        //данные из трудного интерфейса в легкий переносить в ближайших версиях не будем - тут много возни с регулярками
        // скрываем стандартное поле ввода
        step.find('.step-input').addClass("hide");
        step.find('.inputted-formula').addClass('hide');            
        MathJax.Hub.Queue(["Typeset",MathJax.Hub,'einpf'+ step.attr('step-number')]);
        step.find('.toggle_interface').html('переключить на трудный интерфейс');
        
        //если шаг был проверен неправильно, нужно убрать
        remove_wrong_view(step);
                    
    } else {
        // переключение на трудный интерфейс
    	
        //если шаг был проверен неправильно, нужно убрать
        remove_wrong_view(step);

        // скрываем кнопку сброса
        step.find(".reset-easy").addClass("hide");
        step.find('.easy-inputted-formula').addClass('hide');
        
        //переносим данные, введенные в легком интерфейсе            
        var formula = get_formula(step);           
        step.find(".step-input").val(formula);

        // показываем стандартное поле ввода
        step.find('.step-input').removeClass("hide");
        step.find('.inputted-formula').removeClass('hide');

        step.find('.toggle_interface').html('переключить на легкий интерфейс');
        
        //перерисовка визуального представления формулы
        step.find('.step-input').trigger('keyup');     
        
        //сбрасываем все введенное в легком интерфейсе, вдруг опять туда студент переключит и будет чушь
        if (parseInt(step.attr('milestone-id')) > 0){
            step.attr('milestone-id', '0');
            reset_easy_interface();
        }
    };
    //кнопка доступности типа шага
    step_select = step.find('.select-step-type');
    step_select.toggleClass('hide');
    //кнопка доступности подсказок по плану решения - в легком интерфейсе они не нужны
    if (help_enabled == 1){
    	$('#get_plan_hint').toggleClass('hide');
    }  
}

// переключение интерфейсов
$('#notebook-workspace').on('click', '.toggle_interface', function(){
    // выводим предупреждение
	//переключение на легкий интерфейс, студента еще не предупреждали
    if (warning_easy_interface && !$(this).hasClass('easy')) {
        warning_easy_interface = false;
        
        logger.add_to_log('STUDENT TRY CHANGE_INTERFACE TO_EASY ');
        
        modalMessageYesNo('Если вы реализуете возможность работы \в' +
          ' легком интерфейсе, это не позволит претендовать на оценку 5. Переключить?', 'change_interface');

        return false;
    }
    //переключение на легкий интерфейс, студента уже предупреждали
    else if (!$(this).hasClass('easy')){
    	logger.add_to_log('STUDENT CHANGE_INTERFACE TO_EASY ');
    }
    //переключение на трудный интерфейс
    else{
    	logger.add_to_log('STUDENT CHANGE_INTERFACE TO_HARD ');
    }
	change_interface();      
    return false;
});
      
//=========================================================================================================================================
    
//Операции с шагами - трудный интерфейс (ввод данных, исправление)=========================================================================
    
// ввод данных в шаг
$('#notebook-workspace').on('blur', '.null-data', function(){	  
    var data = $(this).val();
    step.attr('data', data);    
    $(this).removeClass('null-data').addClass('fill-data');
    return false;
});
        
//=========================================================================================================================================
    
//Легкий интерфейс - вспомогательные функции (загрузить, сбросить) и операции с шагами =====================================================  

//сброс интерфейса шага
$("#notebook-workspace").on('click', '.reset-easy', function(){
	var step_number = step.find('.step-number').html(); 
	reset_easy_interface();
    logger.add_to_log('STUDENT RESET_EASY_INTERFACE ');
    return false;
});

//=========================================================================================================================================
   
//Подсказки================================================================================================================================ 
    
function show_hints_window(){
	//если открыто окно обозначений, нужно его закрыть
	if ($(".pop-out-notations").hasClass("hide") == false){
		$(".pop-out-notations").slideToggle("medium");
		$(".pop-out-notations").addClass("hide");
	}
	$(".pop-out-hints").slideToggle("medium");
	$(".pop-out-hints").toggleClass("hide");
}

$('body').on('click', '.show_hints_window', function(){
	show_hints_window();
	if ($(".pop-out-hints").hasClass('hide')){
			logger.add_to_log('STUDENT CLOSE_HINTS');
	}
		else{
			logger.add_to_log('STUDENT OPEN_HINTS');
	}
	return false;
});

$('body').on('click', '.show_notations_window', function(){
	if ($(".pop-out-hints").hasClass("hide") == false){
		$(".pop-out-hints").slideToggle("medium");
		$(".pop-out-hints").addClass("hide");
	}
	$(".pop-out-notations").slideToggle("medium");
	$(".pop-out-notations").toggleClass("hide");
	if ($(".pop-out-notations").hasClass('hide')){
		logger.add_to_log('STUDENT CLOSE_NOTATIONS');
}
	else{
		logger.add_to_log('STUDENT OPEN_NOTATIONS');
}
		return false;
});
          
// получить подсказку по шагу по запросу студента
$('#notebook-workspace').on('click', '.get_hint', function(){    
    var type_hint = $(this).attr('data-hint');
    if (help_enabled == 0){
        return false;
    }
    get_hint('student', step, type_hint);
    return false;
});

// получить подсказку по общему ходу решения по запросу студента
$('.notebook-control-bar').on('click', '#get_plan_hint', function(){
	get_plan_hint('student', step);
    return false;
});
  

// ======================== END Логирование ==========================================================================================================
	
//Модуль управления учебным процессом=================================================================================================================
if (task_id !=5 && task_id !=6){
window.setInterval(invoke_tutoring_flow_control, 300000);
//window.setInterval(invoke_tutoring_flow_control, 5000);
}
	function invoke_tutoring_flow_control() {
		if (!waiting && active) {
			progress = get_progress();
			last_correct = get_last_correct();
			milestone_id = step.attr('milestone-id');
			if (milestone_id == 0) {
				milestone_id = get_auto_milestone_id();
			}
			hard_interface = 1;
			if (step.find(".toggle_interface").hasClass("easy")){
				hard_interface = 0;
			}
			if (typeof hints_ids[milestone_id] == 'undefined')
				hints_ids[milestone_id] = Array();
			$.get('/suggest_help',
							{
								'attempt_id' : attempt_id,
								'progress' : progress,
								'last_correct' : last_correct,
								'hard_interface' : hard_interface,
								'milestone_id' : milestone_id,
								'ids' : hints_ids[milestone_id].join(","),
								'situations' : situations.join(","),
								'possible_milestones' : possible_milestones.join(","),
								'combinatorial_milestones' : combinatorial_milestones.join(",")
							},
							function(data) {
								waiting = true;
								reset_progress();
								s_obj = eval('(' + data + ')');
								selected_help = s_obj.selected_help;
								message = s_obj.message;
								sit_number = parseInt(s_obj.situation);
								situations[sit_number - 1] = situations[sit_number - 1] + 1;
								if (selected_help.length > 0) {
									modalMessageYesNo(message, 'help_offer');
								}
								else{
									waiting = false;
								}
							}
		);
	}
	}
	
//==========================END модуль управления учебным процессом===================================================================================
          
//Изменение статуса попытки ==========================================================================================================================

// Срабатывает перед закрытием окна.
$(window).bind('beforeunload', function(){ 
	 closeAttempt();
	 // Отправим оставшиеся логи
	 logger.send_log();
	 //это обработка той ситуации, когда студент хотел закрыть вкладку с попыткой решения задачи, а потом передумал
	 window.setTimeout(revive_attempt, 5000);
	
	  if (try_again==false){
		  return 'Внимание, если вы сейчас закроете страницу, то в следующий раз вам придется заново решать эту задачу.';
	  }
});
   
 $(window).focus(function(){
  	if(!window_focus) {
  	 $.get('/set_active_attempt/' + attempt_id);   	 
  	 //console.log('в фокусе окно');
  	 window_focus = true;
  	 active = true;
  	 logger.add_to_log('STUDENT FOCUS TASK ' + task_id);
  	}
  }).blur(function(){
  	if(window_focus) {
  	 window_focus = false;
  	 active=false;
  	 logger.add_to_log('STUDENT LOST_FOCUS TASK ' + task_id);
  	}
  });
 
//оценка за решение
 $('.notebook-control-bar').on('click', '.btn-success', function(){
		$.get('/get_grade',
				{
					'possible_milestones' : possible_milestones.join(","),
					'combinatorial_milestones' : combinatorial_milestones.join(",")
				},
				function(data) {
			 		s_obj = eval( '('+data+')' );
			     	result = s_obj.result;
			     	grade = s_obj.grade;
			 		modalMessage(result);
			 		logger.add_to_log('STUDENT GET_GRADE ' + grade); 
			 		return false;
		});
 }); 

 //начать попытку заново
 $('.notebook-control-bar').on('click', '#start_again', function(){ 
    logger.add_to_log('STUDENT RESTART_ATTEMPT ' + attempt_id + ' ' + task_id);  
    logger.send_log();
    closeAttempt();    
    //чтобы не появлялось предупреждение перед закрытием страницы
    try_again = true;
    window.location.reload();
 }); 

function closeAttempt() {
	logger.add_to_log("STUDENT END_ATTEMPT " + attempt_id + ' ' + task_id);
	$.get('/close_attempt/' + grade);
}

function revive_attempt(){
	logger.add_to_log("STUDENT STAY_ATTEMPT " + attempt_id + ' ' + task_id);
	$.get('/revive_attempt/' + attempt_id);
}

//=============================END Изменение статуса попытки===========================================================================================
  
//Модальные окна=======================================================================================================================================

//показ сообщения в модальном окне
function modalMessage(text) {
   $("#modal_message").find(".modal-body > p").html(text);
   $("#modal_message").modal();
};

function modalMessageAttention(text) {
   $("#modal_message_attention").find(".modal-body > p").html(text);
   $("#modal_message_attention").modal();
};

//показ сообщения с вопросом Да/Нет в модальном окне
function modalMessageYesNo(text, source) {
	$('#yesNoModal').find('.modal-body > p').html(text);
	$('#yesNoModal').find('.source').html(source);
	$('#yesNoModal').modal();
}

$(".yes-button").click(function(){
	var step_number = step.find('.step-number').html(); 
	source = $(this).parents("#yesNoModal").find(".source").html();
	//обработка согласия/отказа переключиться на легкий интерфейс
    if (source == 'change_interface'){
    	//значит, студент все же захотел переключиться на легкий интерфейс
    	change_interface(); 
        logger.add_to_log('STUDENT CHANGE_INTERFACE TO_EASY ');
    }      
	//обработка согласия принятия помощи от системы
    else if (source == 'help_offer'){
		waiting = false;
	    	if (selected_help != 'change_interface' && selected_help != 'plan_hint'){
		    	get_hint('system', step);
	        }
	        if (selected_help == 'change_interface'){
    			logger.add_to_log('STUDENT AGREE_HELP CHANGE_INTERFACE ');
				change_interface();
				logger.add_to_log('STUDENT RECEIVE_HELP CHANGE_INTERFACE ');
	        }
	        if (selected_help == 'plan_hint'){
	        	get_plan_hint('system', step);
	        }
	}
    else if (source == 'complain'){
    	complain(); 
        logger.add_to_log('STUDENT COMPLAIN ');
    }
    $('#yesNoModal').modal('hide');
	return false;
});
    
//обработка отказа принятия помощи от системы
$(".no-button").click(function(){ 
	var step_number = step.find('.step-number').html(); 
	source = $(this).parents("#yesNoModal").find(".source").html();
	if (source == 'help_offer'){
    		waiting = false;
    		logger.add_to_log('STUDENT REFUSE_HELP ' + selected_help);
    }
	else if (source == 'change_interface'){
        logger.add_to_log('STUDENT CANCEL CHANGE_INTERFACE');
	}
	else if (source == 'complain'){
        logger.add_to_log('STUDENT CANCEL COMPLAIN ' + step_number);
	}
});

//большое окно просмотра решения

$(".unroll-solution").click(function(){
	header_text = $(".unroll-solution").html().replace(/\s/g, '');
	if (header_text == "развернуть"){
		header_text = "свернуть"
		logger.add_to_log('STUDENT OPEN EXTENDED_SOLUTION');
	}
	else{
		header_text = "развернуть"
			logger.add_to_log('STUDENT CLOSE EXTENDED_SOLUTION');
	}
	$(".pop-out-solution").toggleClass("hide");	
	$(".unroll-solution").html(header_text);
	return false;
});

$('#notebook-workspace').on('click', '.close_solution_button', function(){
	$(".pop-out-solution").toggleClass("hide");	
	logger.add_to_log('STUDENT CLOSE EXTENDED SOLUTION');
    return false;
});

//============END Модальные окна ====================================================================================================================== 
  
// Прочие функции======================================================================================================================================
//перетаскивание обозначений для легкого интерфейса
$(".math_toolbox_element").draggable({helper:'clone', appendTo: 'body', zIndex: '1040'});
//===============END Прочие функции ===================================================================================================================
});

function reset_easy_interface(){  
    var milestone_id = step.attr('milestone-id')   
    //заново подгружаем легкий интерфейс
    load_easy_interface(step, milestone_id);
    //если шаг был проверен неправильно - снимаем окраску
    remove_wrong_view(step, false);
}
  
function load_easy_interface(step, milestone_id){
	//сбрасываем введенные студентом данные
    step.find(".send-data").html(""); 
	if (milestone_id == 0){
        step.find(".easy-inputted-formula").html('');
        step.find(".initial-template").html('');   
        step.find(".variables").html(''); 
        step.find(".check-type").html(''); 
        return false;
	}
    // подгружаем легкий интерфейс
    $.get('/get_easy_template/' + milestone_id, function(data){
    	
		s_obj = eval( '('+data+')' );
    	var template = s_obj.template;
    	var variables = s_obj.variables;
    	var check_type = s_obj.check_type;
    	var formula_view = s_obj.formula_view;
    	        	
        step.find(".easy-inputted-formula").html(template);
        step.find(".initial-template").html(template);   
        step.find(".variables").html(variables); 
        step.find(".check-type").html(check_type); 
        //грязновато, но пока так
        step.find(".matrix_digits").css({'vertical-align':'top'});
        
        if (step.find('.toggle_interface').hasClass('easy')) {
        	MathJax.Hub.Queue(["Typeset",MathJax.Hub,'einpf'+ step.attr('step-number')]);
        }
        
        //навешиваем обработчики событий к элементам шаблона шага - обработку драг-н-дропа и т.д.
        enhance(step);
    });
}
    
// навешиваем драг-дроп и прочие дополнительные скрипты на элементы легкого интерфейса
function enhance(step) {
	
	//ввод в текстовое поле - обновление текстового представления
	$("input").focusout(function() {
		var input_data = $(this).val();
		var input_id = $(this).attr('id');
		update_send_data(step, input_id, input_data);
	});
	
	(function(){
		var timer = null;
		var isMouseMoving = false;
		var secondsMoving = 0;

		function mouseStop() { 
			isMouseMoving = false
			secondsMoving = ((new Date).getTime() - secondsMoving)/1000
			var step_number = step.find('.step-number').html(); 
			logger.add_to_log('STUDENT EDIT NUMBER ' + secondsMoving);
		}
	 $('input').keyup(function(e) {
		
		clearTimeout(timer)
		timer = setTimeout(mouseStop, 1000)
		if (!isMouseMoving) {
			isMouseMoving = true
			secondsMoving = (new Date).getTime()
		}
	    });	    
	})();
	
	(function(){
		var timer = null;
		var isMouseMoving = false;
		var secondsMoving = 0;

		function mouseStop() { 
			isMouseMoving = false
			secondsMoving = ((new Date).getTime() - secondsMoving)/1000
			var step_number = step.find('.step-number').html(); 
			logger.add_to_log('STUDENT PRESS_DELETE_OR_BACKSPACE NUMBER ' + secondsMoving);
		}
		
		$('input').focus(function(e){
			remove_wrong_view(step);
		})

		$('input').keydown(function(e) {
		var keyID = e.keyCode;	
		if (keyID == 8 || keyID == 46){
			clearTimeout(timer)
			timer = setTimeout(mouseStop, 1000)
			if (!isMouseMoving) {
				isMouseMoving = true
				secondsMoving = (new Date).getTime()
			}
		}
	   });  
	})();
	   	
    // работа с легким интерфейсом - drop
	$("input").droppable({
        accept: ".math_toolbox_element",
        drop: function(event,ui){
            
        	// Получим tex-представление перетаскиваемого символа
            var tex_value = $(ui.draggable).data('tex');
            // Получим id замещаемого input
            var id = $(this).attr('id');  
            
            //Обновим данные для текстового представления заполняемой формулы
            var send_data = step.find(".send-data").html();
            
            //возможно, до перетаскивания в это текстовое поле было введено значение
            
            var myRegexp = new RegExp('(\\[' + id + '\\]=)(.*?)(;)', 'g');
        	var match = myRegexp.exec(send_data);
            //или заменяем ранее введенное значение, или запоминаем в первый раз
        	if (match != null) {
        		send_data = send_data.replace('[' + id + ']=' + match[2] +';', '[' + id + ']=' + tex_value +';')
        	}
        	else{
        		send_data = send_data + '[' + id + ']=' + tex_value + ';';
        	}
            
            step.find(".send-data").html(send_data); 
			
			// Введем опцинальные дополнительные стили
			// Они будут правильно отображать верхние и нижние индексы
			var symbolPosition = $(this).data('index');
			var additionalAlign = '';
			switch (symbolPosition) {
				case 'upper': additionalStyle = ' font-size: 12px; '; break;
				case 'lower': additionalStyle = ' font-size: 12px; padding-top: 12px; '; break;
				default: additionalStyle = ' font-size: 16px; ';  break;
			}			
			
			var width = $(this).parentsUntil('tr').css('width'); 
			var height = $(this).parentsUntil('tr').css('height'); 
			
			var new_id = step.attr('step-number') + '_' + id;
			$(this).parentsUntil('tr')
				   .replaceWith('<td style=\"' + additionalAlign + '\"><div id=\"' + new_id
						   + '\" style=\"' + additionalStyle
						   + 'height:' + height
						   + '; width:' + width 
						   + '; text-align:center'
						   + ';\">\\(' + tex_value 
						   + '\\)</div></td>');
			
			//перерисовываем с помощью Mathjax
			MathJax.Hub.Queue(["Typeset", MathJax.Hub, new_id]);  
			
			var step_number = step.find('.step-number').html(); 
			
			logger.add_to_log('STUDENT DROP_VALUE ');

        }
    });
}

//функция обновляет данные для текстового представления заполняемой формулы 
function update_send_data(step, id, data){

    var send_data = step.find(".send-data").html();       
    var myRegexp = new RegExp('(\\[' + id + '\\]=)(.*?)(;)', 'g');
	var match = myRegexp.exec(send_data);
    //или заменяем ранее введенное значение, или запоминаем в первый раз
	if (match != null) {
		send_data = send_data.replace('[' + id + ']=' + match[2] +';', '[' + id + ']=' + data +';')
	}
	else{
		send_data = send_data + '[' + id + ']=' + data + ';';
	}
    step.find(".send-data").html(send_data); 
}
