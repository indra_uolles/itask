//для отслеживания прогресса студента за проверяемый период для модуля управления учебным процессом
var curr_correct = 0;
var prev_correct = 0;
var last_correct_time;

//для модуля управления учебным процессом///////////////////////////////////////////////////////////////////////////
function get_progress(){
	progress = curr_correct - prev_correct;
	return progress
}

function reset_progress(){
	prev_correct = curr_correct;
}

function get_time_difference_minutes(earlierDate,laterDate)
{
       var nTotalDiff = laterDate - earlierDate;
       
       var daysDifference = Math.floor(nTotalDiff/1000/60/60/24);
       nTotalDiff -= daysDifference*1000*60*60*24
    
       var hoursDifference = Math.floor(nTotalDiff/1000/60/60);
       nTotalDiff -= hoursDifference*1000*60*60
    
       var minutesDifference = Math.floor(nTotalDiff/1000/60);
       nTotalDiff -= minutesDifference*1000*60
 
       //var secondsDifference = Math.floor(nTotalDiff/1000);       
       return minutesDifference 
}

//сколько минут прошло с момента засчета последнего правильного шага
function get_last_correct()
{
	min_ago = 0;
	if (last_correct_time === undefined){
		min_ago = -1;
	}
	else{
		time_now = new Date().getTime();
		min_ago = get_time_difference_minutes(last_correct_time, time_now);				
	}
	return min_ago
}

//проверка шага, типы проверки: "формула" и "конкретные значения"//////////////////////////////////////////////////

//функция используется для проверки шага, когда его данные могут быть представлены в виде формулы
function check_step_formula(step, task_id){
	
	var result = 0;
	var hard = 1;
	var step_number = step.find('.step-number').html(); 

	var milestone = step.attr("milestone-id");
	
	var easy = step.find(".toggle_interface").hasClass('easy');
	
	if (easy) {
		data = get_formula(step);
		hard = 0;
	} else {
		data = step.find('.step-input').val();
	}  
	
	var send_data = step.find(".send-data").html();

    if ((data == '' && !easy) || (send_data == '' && easy)) {
        modalMessageAttention('Введите данные шага');
        logger.add_to_log('STUDENT CHECK_STEP WRONG EMPTY_DATA ' + step_number);
    } else {
        // все ок отправляем шаг. В ответе data == 0, если шаг неверен, иначе data == id правильного сохранненого в БД шага
        $.get('/send_step',
        	{'milestone': milestone,
        	 'data': data,
        	 'number': step_number,
        	 'hard': hard,
        	 'task_id': task_id,
        	 'possible_milestones': possible_milestones.join(','),       	 
        	 'combinatorial_milestones' : combinatorial_milestones.join(',')
        	},
        function(data) {
    		s_obj = eval( '('+data+')' );
        	result = parseInt(s_obj.result);
        	var inner_milestones = s_obj.inner_milestones;
        	for (i=0; i<=inner_milestones.length-1; i++){
        		if ($.inArray(inner_milestones[i], possible_milestones)==-1){
                	possible_milestones.push(inner_milestones[i]);
        		}
        	}
        	var real_combs = s_obj.real_combs;
        	for (i=0; i<=real_combs.length-1; i++){
        		if ($.inArray(real_combs[i], combinatorial_milestones)==-1){
        			combinatorial_milestones.push(real_combs[i]);
        		}
        	}
        	milestone_id = s_obj.milestone_id;
        	if (result <= 0){
                logger.add_to_log('STUDENT CHECK_STEP WRONG WRONG_FORMULA ' + step_number);
            } else {
                logger.add_to_log('STUDENT CHECK_STEP CORRECT ' + milestone_id + ' ' + step_number);
            }
        	change_step_status(step, result);
        });
    }
    return result
}

function mathjax_to_simple(str){
    matches = str.match(new RegExp('(\\\\begin{pmatrix})(.*?)(\\\\end{pmatrix})', 'g'));
    for (i=0; i<=matches.length-1; i++){
 		old_string = matches[i];
 		new_string = old_string;
 		new_string = new_string.replace('\\begin{pmatrix}', '[');
 		new_string = new_string.replace('\\end{pmatrix}', ']');
  		new_string = new_string.replace(/\\\\/g, ';');
  		new_string = new_string.replace(/&/g, ',');
  		str = str.replace(old_string, new_string);
    }
  	return str
}

//Получим соответствующую шагу формулу - эта функция используется при проверке шага, а также при переключении с легкого интерфейса на трудный
function get_formula (step) {	
	var formula = '';
	if (!parseInt(step.attr('milestone-id')) > 0){
		return '';
	}
	// Получим шаблон формулы
	$.ajax({
		url: '/get_easy_data_check_data/' + step.attr('milestone-id'),
		async: false			
	}).done(function(data){
		s_obj = eval( '('+data+')' );
		check_type = s_obj.check_type;
		formula = '';
		if (check_type == 2){
			formula = s_obj.formula_view;
			formula = mathjax_to_simple(formula);
		} else {
			formula = s_obj.check_data;
		}		
	}); 
	
	//находим все имена переменных шаблона
  var variables = step.find(".variables").html();
  variablesArray = variables.split(';')

  //находим все введенные студентом значения
  var send_data = step.find(".send-data").html();

  for (var i = 0; i < variablesArray.length; i++) {
  	//для каждой переменной находим ее значение, введенное студентом
  	var variableName = variablesArray[i]; 
  	var myRegexp = new RegExp('(\\[' + variableName + '\\]=)(.*?)(;)', 'g');
  	var match = myRegexp.exec(send_data);
  	//заменяем в начальной формуле переменные на их значения
  	if (match != null) {
  		formula = formula.replace('[' + variableName + ']', match[2])
  	}
  }   
  
  //невведенные студентом значения заменяем многоточиями
  formula = formula.replace(/\[(i[0-9]+)]/g, '...');	
  return formula;
  
}

//Функция используется для проверки шага, когда его данные могут быть представлены в виде совокупности значений
function check_step_values(step){
	
	var milestone = step.attr("milestone-id");
	var step_number = step.find('.step-number').html(); 
	
	var values = '';
	var formula_view = '';
	
	// Получим значения для проверки 
	$.ajax({
		url: '/get_easy_data_check_data/' + step.attr('milestone-id'),
		async: false			
	}).done(function(data){
		s_obj = eval( '('+data+')' );
		values = s_obj.check_data;
		formula_view = s_obj.formula_view;
	}); 
	//для каждой переменной сравним ее значение, введенное студентом (если оно есть) и значение, заложенное преподавателем
	//находим все имена переменных шаблона
    var variables = step.find(".variables").html();
    variablesArray = variables.split(';')

    //находим все введенные студентом значения
    var send_data = step.find(".send-data").html();
    
    //вспомогательная переменная - все ли введены значения
    var all_filled = 1;
    //вспомогательная переменная - все ли значения совпадают с заложенными в программе
    var all_matched = 1;
    
    for (var i = 0; i < variablesArray.length; i++) {
    	//для каждой переменной находим ее значение, введенное студентом
    	var variableName = variablesArray[i]; 
    	var expression = '(\\[' + variableName + '\\]=)(.*?)(;)';
    	var myRegexp1 = new RegExp(expression, 'g');
    	var myRegexp2 = new RegExp(expression, 'g');
    	var match1 = myRegexp1.exec(values);
    	var match2 = myRegexp2.exec(send_data);
    	//если студент не ввел какое-то значение, то шаг неверен
    	if (match2 == null) { 
    		all_filled = 0;
    	} 
    	//если введенное студентом значение не совпадает с заложенным значением, то шаг неверен
    	//в админке значения должны вводиться в таком формате: [i1]=2;[i2]=3; - это важно!
    	else {
	    		if (match1[2] !=match2[2]){
	    			all_matched = 0;
	    		}
	    //попутно формируем компактное представление введенного студентом выражения - это для показа в форме попытки и для
	    //показа правильно введенных шагов
	    	formula_view = formula_view.replace('\[' + variableName + '\]', match2[2])
    	}   		
    }
    
    if (all_filled == 0 && !(send_data == '')){
    	logger.add_to_log('STUDENT CHECK_STEP WRONG ' + step_number + ' PARTIALLY_FILLED_DATA');
        change_step_status(step, 0);
   		modalMessageAttention('Дозаполните данные шага');
    } else if (send_data == ''){
    	logger.add_to_log('STUDENT CHECK_STEP WRONG EMPTY_DATA ' + step_number);
        change_step_status(step, 0);
   		modalMessageAttention('Дозаполните данные шага');
    } else if (all_matched == 0){
        logger.add_to_log('STUDENT CHECK_STEP WRONG WRONG_VALUES ' + step_number);
        change_step_status(step, 0);
    	$.get('/save_step_easy_interface', {'milestone': milestone, 'data': formula_view, 'number': step_number, 'result': -1}, function(data){
        });
    } else{
    	// все ок отправляем шаг. В ответе data == 0, если шаг неверен, иначе data == id правильного сохранненого в БД шага
    	$.get('/save_step_easy_interface', {'milestone': milestone, 'data': formula_view, 'number': step_number, 'result': 1}, function(data){
        	result = data;
    		logger.add_to_log('STUDENT CHECK_STEP CORRECT ' + milestone + ' ' +  step_number);
    		change_step_status(step, result);
    	});
    }
}

//функция проверяет, все ли ячейки заполнены в шаблоне ввода шага в легком интерфейсе. Если не все, то значит шаг
//при проверке не сохранялся и жаловаться на него нельзя
function def_all_filled(step){
	all_filled = true;
	var send_data = step.find(".send-data").html();
	var variables = step.find(".variables").html();
    variablesArray = variables.split(';')
	for (var i = 0; i < variablesArray.length; i++) {
		//для каждой переменной находим ее значение, введенное студентом
		var variableName = variablesArray[i]; 
		var expression = '(\\[' + variableName + '\\]=)(.*?)(;)';
		var myRegexp2 = new RegExp(expression, 'g');
		var match2 = myRegexp2.exec(send_data);
		//если студент не ввел какое-то значение, то шаг неверен
		if (match2 == null || match2[2].length == 0) { 
			all_filled = false;
		} 
	}   		
	return all_filled
}

//вид и статус шага////////////////////////////////////////////////////////////////////////////////////////////////
function add_wrong_view(step){
	
    step.attr("step-id", -1);
    step.find('.control-group').removeClass("success").addClass('error');
    if (step.find(".student-step").hasClass('step-correct')) {
        step.find(".student-step").removeClass('step-correct').addClass('step-wrong');
    } else {
        step.find(".student-step").addClass('step-wrong');
    }
    var step_header = step.closest('.step-container').find('h4');
    step_header.css('color', '#B94A48');
    step_header.find('.valid').html(' - введен неверно');
    
    return false;
}

function remove_wrong_view(step, touch_header){
	
	if (typeof touch_header == 'undefined') {
		touch_header = true;
	}
	
	step.find('.control-group').removeClass('error');
	step.find('.student-step').removeClass('step-wrong');
	
	//изменяем заголовок шага
	if (touch_header) {
    	var step_header = step.closest('.step-container').find('h4');
    	step_header.css('color', '#000000');
    	step.find('.title_step').html('');
    	step.find('.valid').html('');
	}
}

//Функция изменяет вид пользовательского интерфейса после проверки шага на правильность
function change_step_status (step, step_id){
    var step_header = step.closest('.step-container').find('h3');
    var step_number = step.find('.step-number').html(); 

    console.log(step_header);

    // on wrong answer
    if (step_id <= 0){    	
    	add_wrong_view(step);
    } else {
    // on correct answer
    	str = '';
    	$.ajax({
    		url: '/update_solution/' + step_id.toString(),
    		async: false			
    	}).done(function(data){
    		str = data;
    	});
    	
        // @TODO: неплохо бы передалать на json.
        // Разделять по переводу строки (\n) -- не очень хорошо,
        // ведь такой перевод строки может быть не один.
    	arr = str.split('\n');
    	str = arr[0] + simple_to_mathjax(arr[1]);
    	var solution_entry = '<a name="step' + parseInt(step_number) + '"></a>' + str;
        // Анимация правильно введенного шага (функция в task_animations.js)
        animation_of_adding_correct_solution(solution_entry);  
        $('#solution').animate({scrollTop: $('#solution')[0].scrollHeight});
        $('#solution-content').append(solution_entry);
        $('#solution-content').animate({scrollTop: $('#solution-content')[0].scrollHeight});
		clear_step_view(step);    	
    	curr_correct = curr_correct + 1;
    	last_correct_time = new Date().getTime();
    }
}

//функция вызывается после признания шага правильным или после жалобы на него. Очищает статус, введенные данные и т.д.
function clear_step_view(step){
		var step_number = step.find('.step-number').html(); 
	    // увеличиваем счетчик шагов
        $('#notebook-workspace').find('.step-number').last().html(parseInt(step_number) + 1);

        //сбрасываем тип шага
        step.attr('milestone-id', '0');
        
    	if (step.find(".toggle_interface").hasClass('easy')) {
    		reset_easy_interface();
    	} else {
            //очищаем выражение, введенное студентом        
            step.find('.step-input').val('');
            // это для того чтобы Mathjax перерисовался
            $('#notebook-workspace').find('.step-input').trigger('keyup');
            $('.step-input').trigger('focus');           
    	}
    	remove_wrong_view(step);
}

//проверка шага, тип проверки: "скрипт". эти функции нужно переделать//////////////////////////////////////////////

//функция используется для проверки того, что в массиве содержится данный элемент
function check_element(array, element){
	result = 0;
	for (i = 0; i <= array.length; i++){
		if (array[i] == element){
			result = 1;
			return result;
		}
	}
	return result;
}

//функция возвращает самый ближайший предшествующий данному шагу шаг с milestone-id из заданного списка
function get_closest_milestone_filtered_step (step, ids_list){
	result = null;
	
	var all_steps = step.parents('#notebook-workspace').children('.step-container:visible');
	var curr_step_index = 0;
	
	all_steps.each(function(index){
		if ($(this).attr('step-number') == step.attr('step-number')){
			curr_step_index = index;
		}
	});
	for (i = 0; i<= curr_step_index; i++){
		if (check_element(ids_list, $(all_steps.get(all_steps.length - 1 -  i)).attr('milestone-id')) == 1){
			result = $(all_steps.get(all_steps.length - 1 -  i));
			return result;
		}
	}
	return result;
}

//Функция используется для проверки шага, когда шаг проверяется с помощью скрипта из админки
function check_step_script(step){
	//получаем список номеров этапов, от которых могут потребоваться данные при проверке текущего шага
	str = '';
	$.ajax({
		url: '/get_easy_data_depend_on/' + step.attr('milestone-id'),
		async: false			
	}).done(function(data){
		str = data;
	});
	
	ids_list = str.split(';;;');
	//если при проверке нужны данные от другого шага, то нужно его найти
	if (ids_list.length > 0) {
	//ищем самый ближайший предшествующий данному шагу шаг с milestone-id из этого списка
	//ссылку на него нужно сохранить для скрипта проверки
		additional_step = get_closest_milestone_filtered_step(step, ids_list);
	} else {
		additional_step = $();
	}
		
	var url =  '/get_easy_data_check_data/' + step.attr('milestone-id');
	
	// Проверим, загружен ли уже такой скрипт, если да, 
	// то не загружаем его заново, а просто выполняем
	var $script = $('head script[src = "'+ url +'"]'); 
	if ($script.length != 0) {
		$script.remove();
	}
	
	var head = document.getElementsByTagName("head")[0];
	var script = document.createElement("script");
    script.src = url;
    head.appendChild(script);    
}