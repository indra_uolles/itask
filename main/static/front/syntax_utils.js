    // Преобразование упрощённого синтаксиса матриц в синтаксис MathJax
    function simple_to_mathjax(str) {
        str = str.replace(/\s/g, '');
        str = str.replace(/(sum\{.*?\}\{.*?\})/g, sum_replacer);
        str = str.replace(/(\[.*?\])/g, matrix_replacer);
        str = str.replace(/frac/g, '\\frac');
        str = str.replace(/sqrt/g, '\\sqrt');
        return str;
    }
    function matrix_replacer(str, offset, s) {
        var out = str.replace(/\[/g, '').replace(/\]/g, '').replace(/,/g, '&')
                .replace(/;/g, '\\\\');
        return '\\begin{pmatrix}' + out + '\\end{pmatrix}';
    }
    function sum_replacer(str, offset, s) {
        var out = str.replace(/sum\{/g, '').replace(/\}\{/g, ';').replace(/=/g, ';').replace(/\}/g, '');
        out = out.split(';');
        if(out.length != 4)
        	return str;
        return '\\sum\\limits_{'+out[0]+'='+out[1]+'}^{'+out[2]+'} ('+out[3]+')';
    }
    
    function light_simple_to_mathjax(str) {
        str = str.replace(/(sum\{.*?\}\{.*?\})/g, sum_replacer);
        str = str.replace(/(\[.*?\])/g, matrix_replacer);
        str = str.replace(/frac/g, '\\frac');
        str = str.replace(/sqrt/g, '\\sqrt');
        return str;
    }