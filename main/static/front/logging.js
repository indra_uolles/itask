function Logger() {
	this.send_log_url = '/write_log';
	this.log = new Array();
	// @INFO: self = this делается для того, чтобы внутри методов класса
	// был доступ к переменным log и send_log_url, т.к. внутри методов
	// this – это уже другой указатель (указатель на сам метод).
	var self = this;		

	// Отправка логов на сервер
	this.send_log = function () {
		if (self.log.length == 0)
			return;
		var data = JSON.stringify(self.log);
		$.post(self.send_log_url, {'data' : data});
		self.log = [];
		return true;
	}

	// Запись в лог
	this.add_to_log = function(data) {
		self.log.push({
			'data' : data,
			'time' : new Date().getTime()
		});

		console.log(data); // For debugging
	}

	// ========== Constructor ==============
	// Отправка логов каждые 30 секунды
	setInterval(this.send_log, 30000);
}
