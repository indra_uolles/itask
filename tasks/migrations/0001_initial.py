# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Notation'
        db.create_table('tasks_notation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('notation', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('tasks', ['Notation'])

        # Adding model 'Task'
        db.create_table('tasks_task', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('tasks', ['Task'])

        # Adding model 'TaskNotation'
        db.create_table('tasks_tasknotation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('task', self.gf('django.db.models.fields.related.ForeignKey')(related_name='task_notations', to=orm['tasks.Task'])),
            ('notation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tasks.Notation'])),
            ('priority', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('tasks', ['TaskNotation'])

        # Adding model 'Milestone_type'
        db.create_table('tasks_milestone_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal('tasks', ['Milestone_type'])

        # Adding model 'Easy_interface_setting'
        db.create_table('tasks_easy_interface_setting', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('view', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('variables', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('check_data', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('check_type', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('formula_view', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('tasks', ['Easy_interface_setting'])

        # Adding model 'Milestone'
        db.create_table('tasks_milestone', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tasks.Milestone_type'], null=True, blank=True)),
            ('is_multiple', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('hard_data', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('easy_interface_setting', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tasks.Easy_interface_setting'], null=True, blank=True)),
            ('plan_hint', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('tasks', ['Milestone'])

        # Adding M2M table for field depends_on on 'Milestone'
        db.create_table('tasks_milestone_depends_on', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_milestone', models.ForeignKey(orm['tasks.milestone'], null=False)),
            ('to_milestone', models.ForeignKey(orm['tasks.milestone'], null=False))
        ))
        db.create_unique('tasks_milestone_depends_on', ['from_milestone_id', 'to_milestone_id'])

        # Adding model 'EquivRel'
        db.create_table('tasks_equivrel', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('task', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tasks.Task'])),
        ))
        db.send_create_signal('tasks', ['EquivRel'])

        # Adding M2M table for field equivalent_milestones on 'EquivRel'
        db.create_table('tasks_equivrel_equivalent_milestones', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('equivrel', models.ForeignKey(orm['tasks.equivrel'], null=False)),
            ('milestone', models.ForeignKey(orm['tasks.milestone'], null=False))
        ))
        db.create_unique('tasks_equivrel_equivalent_milestones', ['equivrel_id', 'milestone_id'])

        # Adding model 'Expression'
        db.create_table('tasks_expression', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('milestone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tasks.Milestone'])),
            ('easy_interface_setting', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tasks.Easy_interface_setting'], null=True, blank=True)),
        ))
        db.send_create_signal('tasks', ['Expression'])

        # Adding model 'Hint'
        db.create_table('tasks_hint', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hint_type', self.gf('django.db.models.fields.IntegerField')()),
            ('hint_kind', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('milestone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tasks.Milestone'], null=True, blank=True)),
            ('idx', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('for_easy', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('tasks', ['Hint'])

        # Adding model 'Solution'
        db.create_table('tasks_solution', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('task', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tasks.Task'])),
            ('for_hints', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('not_necessary', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('tasks', ['Solution'])

        # Adding M2M table for field milestones on 'Solution'
        db.create_table('tasks_solution_milestones', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('solution', models.ForeignKey(orm['tasks.solution'], null=False)),
            ('milestone', models.ForeignKey(orm['tasks.milestone'], null=False))
        ))
        db.create_unique('tasks_solution_milestones', ['solution_id', 'milestone_id'])

        # Adding model 'SolutionMilestone'
        db.create_table('tasks_solutionmilestone', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('solution', self.gf('django.db.models.fields.related.ForeignKey')(related_name='solution_milestones', to=orm['tasks.Solution'])),
            ('milestone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tasks.Milestone'])),
            ('priority', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('tasks', ['SolutionMilestone'])


    def backwards(self, orm):
        # Deleting model 'Notation'
        db.delete_table('tasks_notation')

        # Deleting model 'Task'
        db.delete_table('tasks_task')

        # Deleting model 'TaskNotation'
        db.delete_table('tasks_tasknotation')

        # Deleting model 'Milestone_type'
        db.delete_table('tasks_milestone_type')

        # Deleting model 'Easy_interface_setting'
        db.delete_table('tasks_easy_interface_setting')

        # Deleting model 'Milestone'
        db.delete_table('tasks_milestone')

        # Removing M2M table for field depends_on on 'Milestone'
        db.delete_table('tasks_milestone_depends_on')

        # Deleting model 'EquivRel'
        db.delete_table('tasks_equivrel')

        # Removing M2M table for field equivalent_milestones on 'EquivRel'
        db.delete_table('tasks_equivrel_equivalent_milestones')

        # Deleting model 'Expression'
        db.delete_table('tasks_expression')

        # Deleting model 'Hint'
        db.delete_table('tasks_hint')

        # Deleting model 'Solution'
        db.delete_table('tasks_solution')

        # Removing M2M table for field milestones on 'Solution'
        db.delete_table('tasks_solution_milestones')

        # Deleting model 'SolutionMilestone'
        db.delete_table('tasks_solutionmilestone')


    models = {
        'tasks.easy_interface_setting': {
            'Meta': {'object_name': 'Easy_interface_setting'},
            'check_data': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'check_type': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'formula_view': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'variables': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'view': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        'tasks.equivrel': {
            'Meta': {'object_name': 'EquivRel'},
            'equivalent_milestones': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['tasks.Milestone']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'task': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Task']"})
        },
        'tasks.expression': {
            'Meta': {'object_name': 'Expression'},
            'easy_interface_setting': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Easy_interface_setting']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'milestone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Milestone']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        'tasks.hint': {
            'Meta': {'ordering': "['hint_type', 'idx']", 'object_name': 'Hint'},
            'for_easy': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'hint_kind': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'hint_type': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idx': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'milestone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Milestone']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'tasks.milestone': {
            'Meta': {'object_name': 'Milestone'},
            'depends_on': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'depends_on_rel_+'", 'null': 'True', 'to': "orm['tasks.Milestone']"}),
            'easy_interface_setting': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Easy_interface_setting']", 'null': 'True', 'blank': 'True'}),
            'hard_data': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_multiple': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'plan_hint': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Milestone_type']", 'null': 'True', 'blank': 'True'})
        },
        'tasks.milestone_type': {
            'Meta': {'object_name': 'Milestone_type'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'tasks.notation': {
            'Meta': {'object_name': 'Notation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'notation': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'tasks.solution': {
            'Meta': {'object_name': 'Solution'},
            'for_hints': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'milestones': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['tasks.Milestone']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'not_necessary': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'task': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Task']"})
        },
        'tasks.solutionmilestone': {
            'Meta': {'ordering': "['priority']", 'object_name': 'SolutionMilestone'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'milestone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Milestone']"}),
            'priority': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'solution': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'solution_milestones'", 'to': "orm['tasks.Solution']"})
        },
        'tasks.task': {
            'Meta': {'ordering': "['name']", 'object_name': 'Task'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'tasks.tasknotation': {
            'Meta': {'ordering': "['priority']", 'object_name': 'TaskNotation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Notation']"}),
            'priority': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'task': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'task_notations'", 'to': "orm['tasks.Task']"})
        }
    }

    complete_apps = ['tasks']