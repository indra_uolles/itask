# -*- coding: utf-8 -*-

from django.db import models
from support.models import Theory
# Create your models here.



class Notation(models.Model):    
    name = models.CharField(max_length=255, verbose_name='Название')
    notation = models.CharField(max_length=255, verbose_name='Значение')    

    def __unicode__(self):
        return u'%s <=> %s' % (self.name, self.notation)

class Task(models.Model):
    name = models.CharField(blank=True, null=True, max_length=255, verbose_name='Название')
    description = models.TextField(blank=True, null=True, verbose_name='Условие')

    def __unicode__(self):
        return u'Задача "%s"' % self.name
    
    class Meta:
        ordering = ['name']

class TaskNotation(models.Model):
    task = models.ForeignKey(Task, verbose_name='Задача', related_name='task_notations')
    notation = models.ForeignKey(Notation, verbose_name='Обозначение')
    priority = models.IntegerField(default=0)

    class Meta:
        ordering = ['priority']

class Milestone_type(models.Model):
    name = models.CharField(blank=True, null=True, max_length=255, verbose_name='Название')

    def __unicode__(self):
        return self.name
    
class Easy_interface_setting(models.Model):
    name = models.CharField(blank=True, max_length=255, verbose_name='Название')
    view = models.TextField(blank=True, verbose_name='Визуальное представление легкого интерфейса' )
    variables = models.TextField(blank=True, verbose_name='Переменные легкого интерфейса' ) 
    check_data = models.TextField(blank=True, verbose_name='Значения/формула/скрипт для проверки легкого интерфейса' )
    CHECK_TYPE = [
        (1, 'Формула'),
        (2, 'Конкретные значения'),
        (3, 'Скрипт'),
    ]
    check_type = models.IntegerField(choices=CHECK_TYPE, default=1, verbose_name='Тип проверки легкого интерфейса')
    formula_view = models.TextField(blank=True, verbose_name='Формула для компактного отображения введенного шага' )

    def __unicode__(self):
        return u'Этап "%s"' % self.name
       
class Milestone(models.Model):
    name = models.CharField(blank=True, null=True, max_length=255, verbose_name='Название')
    type = models.ForeignKey(Milestone_type, blank=True, null=True, verbose_name='Тип')
    is_multiple = models.BooleanField(blank=True, default=False, verbose_name='Комбинаторный этап')
    hard_data = models.TextField(blank=True, null=True, verbose_name='Данные для трудного интерфейса')
    easy_interface_setting = models.ForeignKey(Easy_interface_setting, blank=True, null=True, verbose_name='Настройка легкого интерфейса')
    plan_hint = models.TextField(blank=True, null=True, verbose_name='Подсказка по ходу решения задачи')
    depends_on = models.ManyToManyField("self", blank=True, null=True, verbose_name='От каких этапов зависит проверка')

    def __unicode__(self):
        return u'Этап "%s"' % self.name
    
class EquivRel(models.Model):
    name = models.CharField(blank=True, null=True, max_length=255, verbose_name="Название")
    task = models.ForeignKey(Task, verbose_name='Задача')
    equivalent_milestones = models.ManyToManyField(Milestone, blank=True, null=True)
    
    def __unicode__(self):
        return u'Эквивалентность "%s"' % self.name
    
class Expression(models.Model):
    text = models.TextField(verbose_name='Текст выражения')
    milestone = models.ForeignKey(Milestone, verbose_name='Этап')
    easy_interface_setting = models.ForeignKey(Easy_interface_setting, blank=True, null=True, verbose_name='Настройка легкого интерфейса')

    def __unicode__(self):
        return u'Выражение "%s"' % self.text


class Hint(models.Model):
    TYPE_CHOICES = [
        (1, 'Теоретическая'),
        (2, 'Схематическая'),
        (3, 'Конкретная'),
    ]
    KIND_CHOICES = [
        (1, 'короткая подсказка'),
        (2, 'ссылка на теоретический материал'),
        (3, 'ссылка на задачу для переключения'),
    ]
    hint_type = models.IntegerField(choices=TYPE_CHOICES, verbose_name=u'Тип подсказки')
    hint_kind = models.IntegerField(choices=KIND_CHOICES, verbose_name=u'Вид подсказки', default=1)
    milestone = models.ForeignKey(Milestone, blank=True, null=True, verbose_name='Этап')
    idx = models.IntegerField(blank=True, null=True, verbose_name='Очередность')
    text = models.TextField(blank=True, null=True, verbose_name='Текст подсказки')
    for_easy = models.BooleanField(default=True, verbose_name='Актуальна при легком интерфейсе')

    def get_hint_save(self, student):
        #attempts = Attempt.objects.filter(student=student, task=self.hint_task)
        sessions = [ s for s in student.sessions.all() ]
        attempts = [attempt for attempt in self.hint_task.attempts.filter(session__in=sessions)]

        if attempts.count() > 0:
            return attempts[attempts.count()-1]
        else:
            return None

    class Meta:
        ordering = ['hint_type','idx']

class Solution(models.Model):
    milestones = models.ManyToManyField(Milestone, blank=True, null=True)
    name = models.CharField(blank=True, null=True, max_length=255, verbose_name="Название")
    task = models.ForeignKey(Task, verbose_name='Задача')
    for_hints = models.BooleanField(verbose_name='Использовать решения для подсказок', default=False)
    not_necessary = models.BooleanField(verbose_name='Решение с необязательными этапами', default=False)

    def __unicode__(self):
        return u'Решение "%s"' % self.name
    
class SolutionMilestone(models.Model):
    solution = models.ForeignKey(Solution, verbose_name='Решение', related_name='solution_milestones')
    milestone = models.ForeignKey(Milestone, verbose_name='Этап')
    priority = models.IntegerField(default=0)

    class Meta:
        ordering = ['priority']