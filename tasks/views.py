# coding: utf-8
import json
import string

from django.core.context_processors import csrf
from django.http import Http404, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.utils.encoding import smart_str


from tasks.models import Task, Milestone, Solution, Hint, Expression, Easy_interface_setting, SolutionMilestone, EquivRel
from students.models import Student, Session, Attempt, Step, Log, Saving
from tasks.utils import get_hint_save, get_int_arr_from_GET
from analyzer.analyzer import Analyzer
import math
import datetime

HINT_TYPES = {
    1: u'теоретическая',
    2: u'схематическая',
    3: u'конкретная',
}

def load_saving(request, saving_id):
    """ Загрузка сохраненного решения"""

    saving = Saving.objects.get(id=saving_id)
    attempt = saving.attempt
    student = attempt.session.student
    task = attempt.task
    task_params = attempt.task_parameters
    help_enabled = 0
    if (attempt.help_enabled):
        help_enabled = 1

    milestones = Milestone.objects.all()

    savings = Saving.objects.filter(attempt__session__student=student)

    data = {
        'task': task,
        'student_id': student.id,
        'milestones': milestones,
        'help_enabled': help_enabled,
        'task_params': task_params,
        'savings': savings,
        'attempt_save': saving,
        }

    return render_to_response("save.html", data)

def set_active_attempt(request, attempt_id):
    '''функция создана потому, что у студента может быть одновременно открыто несколько вкладок решения задач'''
    session = Session.objects.get(id=int(request.session['student_session_id']))
    attempt = Attempt.objects.get(id=int(attempt_id))
    request.session['attempt'] = attempt.id
    return HttpResponse('')

@login_required
def solve_task(request, task_id):
    """ Начальная вьюха решения задачи """

    try:
        task = Task.objects.get(id=int(task_id))
    except Exception:
        raise Http404
    
    session = Session.objects.get(id=int(request.session['student_session_id']))
    student = session.student
    
    attempt = Attempt(session=session, task=task, task_parameters='', help_enabled=True)
    attempt.save()
    request.session['attempt'] = attempt.id
    
    SolutionMilestones = None

    #отбор только по тем этапам, которые присутствуют в решении системы, рекомендованном преподавателем
    rec_solutions = Solution.objects.filter(task=task, for_hints=True)
    #таких рекомендованных решений может быть два. В этом случае нужен доп. отбор по наличествующим настройкам легкого интерфейса
    if (rec_solutions.count() > 1):
        solution_id = 0
        max_count = 0
        for rec_solution in rec_solutions:
            milestones = Milestone.objects.filter(solution__in=rec_solutions).distinct() 
            easy_interface_settings = Easy_interface_setting.objects.filter(milestone__in=milestones)
            if (easy_interface_settings.count() > max_count):
                max_count = easy_interface_settings.count()
                solution_id = rec_solution.id 
        if (solution_id > 0):
            SolutionMilestones = SolutionMilestone.objects.filter(solution=Solution.objects.get(id=solution_id))              
    else:
        SolutionMilestones = SolutionMilestone.objects.filter(solution__in=rec_solutions) 

    savings = Saving.objects.filter(attempt__session__student=student)
    help_enabled = 0
    if (attempt.help_enabled):
        help_enabled = 1
        
    attempt_id = attempt.id

    data = {
        'attempt_id': attempt_id,
        'task': task,
        'student_id': student.id,
        'solution_milestones': SolutionMilestones,
        'help_enabled': help_enabled,
         'task_params': '',
        'savings': savings,
    }

    return render_to_response("task.html", data,
                              context_instance=RequestContext(request))

def get_solution_id(attempt, possible_milestones, combinatorial_milestones):
    '''возвращение id решения, по которому скорее всего следует студент'''
    task = attempt.task
    student_steps = Step.objects.filter(attempt=attempt, status=1).order_by('time') 
      
    solution_id = 0    
    if (student_steps.count() == 0):
    #если студент еще не вводил никаких шагов, то возвращаем рекомендуемое решение
        solution_id = Solution.objects.filter(task=task,for_hints=True)[0].id
    else: 
        solution_data = []
        an = Analyzer()
        stud_milestones_ids = get_stud_milestones(student_steps, possible_milestones, combinatorial_milestones)
        solutions = Solution.objects.filter(task=task, not_necessary=False)

        for solution in solutions:
            milestones_ids = get_milestone_ids(solution)  
            simil = an.dict_simil(stud_milestones_ids, milestones_ids)
            milestones = Milestone.objects.filter(solution=solution) 
            solution_data.append([simil, milestones.count(), solution.id])
            
        rec_id = Solution.objects.filter(task=task,for_hints=True)[0].id        
        row = get_filtered_row(solution_data, rec_id)            
        solution_id = solution_data[row][2]
    
    return solution_id

def get_stud_milestones(student_steps, possible_milestones, combinatorial_milestones):
    '''формируем множество этапов, которые точно реализованы в решении студента и 
    которые могут там быть реализованы'''
    stud_milestones_ids = []
    for student_step in student_steps:
        stud_milestone = student_step.milestone
        if (stud_milestone != None):               
            stud_milestones_ids.append(stud_milestone.id)
            
    for i in range(len(possible_milestones)):
        if (not (possible_milestones[i] in stud_milestones_ids)):
            stud_milestones_ids.append(possible_milestones[i]) 

    for i in range(len(combinatorial_milestones)):
        if (not (combinatorial_milestones[i] in stud_milestones_ids)):
            stud_milestones_ids.append(combinatorial_milestones[i]) 
            
    return stud_milestones_ids

def get_milestone_ids(solution):
    '''формируем список id этапов задачи'''
    milestones = SolutionMilestone.objects.filter(solution=solution)
    milestones_ids = []
    for m in milestones:
        milestones_ids.append(m.milestone.id)
    return milestones_ids

def get_milestone_id(request):
    '''
     Автоматическое определение типа шага, для выдачи подсказок 
     по текущему шагу по сути, принцип такой же, как и для подсказок
     по плану решения только вызывается оно с клиента сразу'''
    attempt = Attempt.objects.get(id=int(request.session['attempt'])) 
    possible_milestones = get_int_arr_from_GET('possible_milestones', request)
    combinatorial_milestones = get_int_arr_from_GET('combinatorial_milestones', request)            
    milestone_id = get_plan_hint_milestone_id(attempt, possible_milestones, combinatorial_milestones)
    data = {
            'milestone_id': milestone_id,
    }
    s = json.dumps(data)
    return HttpResponse(s)       
            
def get_last_realized_milestone(task, solution, student_steps, possible_milestones, combinatorial_milestones):
    '''функция ищет первый упорядоченный этап, который еще не был реализован студентом'''
    found_index = -1
    app_arr = get_app_arr(task, solution, student_steps, possible_milestones, combinatorial_milestones)
    #теперь надо найти самую последнюю 1 в app_arr, это и будет то место, на котором
    #остановился студент
    found_index = -1
    for i in range(len(app_arr)):
        if (app_arr[i] == 1):
            found_index = i           
    #одинаково формируются множества id этапов и упорядоченных этапов
    #так что можно брать этот индекс и возвращать его        
    return found_index

def get_app_arr(task, solution, student_steps, possible_milestones, combinatorial_milestones):
    '''функция для конкретного решения возвращает массив с 1 и 0, где 1 - если этап, соответствующий строке, был реализован в решении
    студента, 0 в противном случае'''
    stud_milestones = get_stud_milestones(student_steps, possible_milestones, combinatorial_milestones)
    sol_milestones = get_milestone_ids(solution) 
    app_arr = [] 
    for i in range(len(sol_milestones)):
        if (sol_milestones[i] in stud_milestones):
            app_arr.append(1)
        else:
            app_arr.append(0)
                
    return app_arr

def get_grade(request):
    attempt = Attempt.objects.get(id=int(request.session['attempt']))
    task = attempt.task
    possible_milestones = get_int_arr_from_GET('possible_milestones', request)
    combinatorial_milestones = get_int_arr_from_GET('combinatorial_milestones', request)
    solution_id = get_solution_id(attempt, possible_milestones, combinatorial_milestones)
    if (solution_id > 0):
        solution = Solution.objects.get(id=solution_id)
        student_steps = Step.objects.filter(attempt=attempt, status=1).order_by('time')
        weight_arr = {}
        grade_el = 0
        grade = 0
        for step in student_steps:
            #если шаг студента с неопределенным типом, такое могло произойти только если студент вводил шаг в трудноим интерфейсе. Мы это учтем позже
            if (step.milestone != None):                               
                if (step.interface==1):
                    weight_arr[step.milestone.id] = 1
                else:
                    weight_arr[step.milestone.id] = 0.5
        solution_milestones = SolutionMilestone.objects.filter(solution=solution)
        n = solution_milestones.count()
        try:
            grade_el = 100/float(n)
        except Exception:
            result = 'К сожалению, преподаватель плохо настроил проверку решения этой задачи. Обратитесь к нему'  
        app_arr = get_app_arr(task, solution, student_steps, possible_milestones, combinatorial_milestones)
        #мы можем посмотреть на то, реализован ли каждый этап. если это комбинаторный этап, и его нету среди шагов
        #студента, значит он был введен альтернативным способом, а это возможно только в трудном интерфейсе       
        index = 0
        for sm in solution_milestones:
            milestone_id = sm.milestone.id
            if (app_arr[index] == 1):
                if (not (weight_arr.has_key(milestone_id))):
                    grade = grade + grade_el
                else:                                  
                    grade = grade + weight_arr[milestone_id]*grade_el
            index = index + 1
                
        st_grade = '0' 
        if (grade>100):
            grade = 100
        if (math.fabs(grade-100)<5):
            grade = 100
        if (grade>0):
            st_grade = str(int(grade))
            
        result = 'Ваша оценка: ' + st_grade + '%'                         
    else:
        result = 'Не удалось понять ваше решение, оно будет оценено преподавателем'
         
    data = {
            'result': str(result),
            'grade': grade,
    }
    s = json.dumps(data)         
    return HttpResponse(s)


def get_plan_hint_milestone_id(attempt, possible_milestones, combinatorial_milestones):
    """ Получение подсказки по плану решения """

    task = attempt.task
    student_steps = Step.objects.filter(attempt=attempt, status=1).order_by('time')
    take_rec = False
    found_index = 0
    
    if (student_steps.count() == 0):
        take_rec = True
    else:
        #Смотрим, сколько шагов студента соответствуют этапам из решений "с необязательными этапами" 
        unnec_solutions = Solution.objects.filter(task=task, not_necessary=True)
        unnec_milestones = Milestone.objects.filter(solution__in=unnec_solutions).distinct()
        unnec_steps = student_steps.filter(milestone__in=unnec_milestones)
        if (student_steps.count() == unnec_steps.count()):
            take_rec = True
            
    solution_id = 0
      
    if (take_rec):
        #таких рекомендуемых решений может быть 2 и больше, в этом случае нужно производить доп. отбор
        rec_solutions = Solution.objects.filter(task=task, for_hints=True)
        if (len(rec_solutions) > 1 and student_steps.count() == 0):
            solution_id = 0
            max_count = 0
            for rec_solution in rec_solutions:
                milestones = Milestone.objects.filter(solution__in=rec_solutions).distinct() 
                easy_interface_settings = Easy_interface_setting.objects.filter(milestone__in=milestones)
                if (easy_interface_settings.count() > max_count):
                    max_count = easy_interface_settings.count()
                    solution_id = rec_solution.id 
        
        elif (len(rec_solutions) == 1):
            solution_id = rec_solutions[0].id 
        else:
            an = Analyzer()
            max_simil = 0
            stud_milestones_ids = get_stud_milestones(student_steps, possible_milestones, combinatorial_milestones)
            for solution in rec_solutions:
                milestones_ids = get_milestone_ids(solution)  
                simil = an.dict_simil(stud_milestones_ids, milestones_ids)
                if (simil > max_simil):
                    max_simil = simil
                    solution_id = solution.id

    else:         
        solution_id = get_solution_id(attempt, possible_milestones, combinatorial_milestones)
                
    solution_milestones = None
    # найдено решение близкое решению студента 
    
    if (student_steps.count() == 0 or (student_steps.count() == unnec_steps.count() and len(combinatorial_milestones) == 0)):
        solution = Solution.objects.get(id=solution_id)
        solution_milestones = SolutionMilestone.objects.filter(solution=solution) 
        return solution_milestones[0].milestone.id 
    else:                     
        if (solution_id > 0): 
            solution = Solution.objects.get(id=solution_id)
            solution_milestones = SolutionMilestone.objects.filter(solution=solution)    
            found_index = get_last_realized_milestone(task, solution, student_steps, possible_milestones, combinatorial_milestones)       
                       
            if (found_index == solution_milestones.count() - 1):
                return -1 
                
        if (found_index == -1):
            return -2  
        else:
            #если студент уже ввел какие-то шаги, то нужно подсказывать следующий этап
            if (student_steps.count() > 0):
                found_index = found_index + 1           
            return solution_milestones[found_index].milestone.id   

#получение подсказки по плану решения по запросу студента
def get_plan_hint(request):
    """ Подсказка по общему ходу решения (сильная) """
    attempt = Attempt.objects.get(id=int(request.session['attempt']))
    possible_milestones = get_int_arr_from_GET('possible_milestones', request)
    combinatorial_milestones = get_int_arr_from_GET('combinatorial_milestones', request)
    milestone_id = get_plan_hint_milestone_id(attempt, possible_milestones, combinatorial_milestones)
    step_number = request.GET.get('step_number', 0)
    if (milestone_id == 0):
        return HttpResponse('')
    elif (milestone_id == -1):
        return HttpResponse('finish')
    else:
        #если преподаватель заполнил текст подсказки, то берем его, иначе имя типа берем
        name = ''
        milestone = Milestone.objects.get(id=milestone_id)
        if (len(xstr(milestone.plan_hint)) > 0):
            name = milestone.plan_hint
        else:
            name = u'В качестве следующего шага нужно ввести такой:' + milestone.type.name
        return render_to_response("hint.html", {'hint_text': xstr(name), 'hint_id': milestone_id, 'hint_type': 4, 'hint_kind': 1, 'step_number': step_number})  

    return HttpResponse('')

# получение id подсказки - используется в модуле упр уч процессом, чтобы понять, есть ли такая
# подсказка, а также для получения подсказки по запросу студента или по инициативе системы

def get_filtered_hint_id(hints_ids, milestone_id, easy, hint_type, hint_filter):    
    ids = []
    for hint_id in string.split(hints_ids, ","):
        if hint_id:
            ids.append(int(hint_id))

    milestone = Milestone.objects.get(id=int(milestone_id))

    #получаем подсказки, которые студент еще не видел у этого этапа
    # фильтр - 0 - нет фильтра, 1 - краткая, 2 - теория, 3 - переключиться к задаче
    if (hint_filter == 0):
    # надо переименовать этот флажок, он должен быть "не актуальна для легкого интерфейса"
        if easy:
            hints = Hint.objects.filter(milestone=milestone, hint_type=hint_type, for_easy=True).exclude(id__in=ids)
        else:
            hints = Hint.objects.filter(milestone=milestone, hint_type=hint_type).exclude(id__in=ids)        
    else:        
        if easy:
            hints = Hint.objects.filter(milestone=milestone, hint_type=hint_type, hint_kind=hint_filter, for_easy=True).exclude(id__in=ids)
        else:
            hints = Hint.objects.filter(milestone=milestone, hint_type=hint_type, hint_kind=hint_filter).exclude(id__in=ids)
        
    if (hints.count() == 0):
        return 0
    
    hint = hints[0]
     
    return hint.id

def get_filtered_hint(request):
    attempt = Attempt.objects.get(id=int(request.session['attempt']))
    hints_ids = request.GET.get('hints_ids', '')
    milestone_id = request.GET.get('milestone_id', '')
    easy = bool(int(request.GET.get('easy', 0)))
    hint_type = int(request.GET.get('hint_type', ''))
    hint_filter = int(request.GET.get('hint_filter', ''))
    step_number = request.GET.get('step_number', '')
    hint = None
    hint_id = get_filtered_hint_id(hints_ids, milestone_id, easy, hint_type, hint_filter)
    if (hint_id > 0):
        hint = Hint.objects.get(id=hint_id)
        # добавляем подсказку к попытке
        attempt.hints.add(hint)
        attempt.save()                
        return render_to_response("hint.html", {'hint_text': hint.text, 'hint_id': hint.id, 'hint_type': hint_type, 'hint_kind': hint.hint_kind, 'step_number': step_number})

    return HttpResponse('')
 
# для того, чтобы все время не лезть в базу данных за данными одних и тех же этапов - для проверки шага далее

class InnerMilestone:
    def __init__(self, id, name, hard_data):
        self.id = id
        self.name = name
        self.hard_data = hard_data
    def __str__(self):
        return '[%s, %s;]' % (self.id, self.name)

def fill_im_arr(an, student_steps, im_arr, inner_milestones, solution_ids, possible_milestones, combinatorial_milestones):
    '''функция для каждого этапа помещает в массив максим близость среди решений, в которых присутствует
    этот этап, к решению студента, длину и id соответствующего решения'''
    for key, value in inner_milestones.iteritems():
        sol_data = []   
        sol_arr = solution_ids[key]     
        for i in range(len(sol_arr)):           
            solution = Solution.objects.get(id=sol_arr[i])
            stud_milestones_ids = get_stud_milestones(student_steps, possible_milestones, combinatorial_milestones)
            milestones_ids = get_milestone_ids(solution)  
            similarity = an.dict_simil(stud_milestones_ids, milestones_ids)            
            sol_milestones = Milestone.objects.filter(solution=solution)                   
            #для каждого решения нужно запомнить его близость, длину, id
            sol_data.append([similarity, len(sol_milestones), solution.id])
            
        row = 0
        if (len(sol_data) > 1):
            row = get_filtered_row(sol_data)
            
        arr= [sol_data[row][0], sol_data[row][1], sol_data[row][2]]       
        im_arr[key] = arr
            
    return im_arr

def get_filtered_row(sol_data, rec_id = 0):
    row = 0
    max_similarity = 0
    
    # смотрим,какая максимальная близость к решению студента среди всех решений, в которых
    #встречается данный этап
    for i in range(len(sol_data)):
        similarity = sol_data[i][0]
        if (similarity > max_similarity):
            max_similarity = similarity
    
    rec_row = 0
    if (max_similarity > 0):
        #смотрим, сколько решений обладают максимальной близостью к решению студента
        max_count = 0
        for i in range(len(sol_data)):
            similarity = sol_data[i][0]
            if (similarity == max_similarity):
                max_count = max_count + 1
                row = i
            # если среди всех решений с макс близостью окажется рекомендуемое, то нужно будет брать его
                if (sol_data[i][2] == rec_id):
                    rec_row = i
                
        if (max_count > 1):
            #если нашлось несколько решений, нужно наложить доп. отбор по минимальной длине
            #если будет несколько решений с одинаковой длиной, возьмем последнее
            
            if (rec_row > 0):
                row = rec_row
            else:
                min_len = 1000
                for i in range(len(sol_data)):
                    similarity = sol_data[i][0]
                    if (similarity == max_similarity):
                        length = sol_data[i][1]
                        if (length <= min_len):
                            min_len = length
                            row = i
    
    return row

def get_filtered_milestone_id(an, inner_milestones, attempt, solution_ids, possible_milestones, combinatorial_milestones):
    '''функция, подбирающая наиболее подходящий этап из нескольких этапов, эквивалентных шагу студента'''
    milestone_id = 0
    im_arr = {}
    for key in inner_milestones.iterkeys():
        im_arr[key]=[0, 0, 0]
        
    student_steps = Step.objects.filter(attempt=attempt, status=1).order_by('time')
    if (student_steps.count() > 0): 
        im_arr = fill_im_arr(an, student_steps, im_arr, inner_milestones, solution_ids, possible_milestones, combinatorial_milestones)           
                            
        arr = []
        for key, value in im_arr.iteritems():
            in_arr = im_arr[key]
            in_arr.append(key)           
            arr.append(in_arr)
        
        row = 0
        if (len(arr) > 0):
            row = get_filtered_row(arr)
            
        if (arr[row][0] > 0):
            milestone_id = arr[row][3]
       
    return milestone_id
            
# проверяем шаг

def check_step(request):
    attempt = Attempt.objects.get(id=int(request.session['attempt']))
    milestone_id = int(request.GET.get('milestone', 0))
    number = request.GET.get('number', 0)
    step_data = request.GET.get('data', '')
    hard = int(request.GET.get('hard', '0'))
    task_id = request.GET.get('task_id', 0)
    notations = [task_notation.notation.name for task_notation in attempt.task.task_notations.all()]
    attempt.save()
    required_expression = None 
    an = Analyzer()
    milestone = None
    result = -1
    s=''
    possible_milestones = get_int_arr_from_GET('possible_milestones', request)
    real_combs = get_int_arr_from_GET('combinatorial_milestones', request)

    try:
        task = Task.objects.get(id=task_id)
    except Exception:
        return HttpResponse('error')
    
    inner_milestones = {}
    solution_ids = {}
    
    if (milestone_id == 0):
        
        #автоматическое определение этапа, которому соответствует шаг студента 
        #отбираем этапы те и только те, которые встречаются в возможных решениях данной задачи
        solutions = Solution.objects.filter(task=task)
        for solution in solutions:
            milestones = Milestone.objects.filter(solution=solution)
            for m in milestones:
                if (an.type_selection_is_candidate(m.hard_data, step_data, notations)):
                    if (an.check_equivalence(notations, step_data, m.hard_data)):
                        # запоминаем этот этап
                        if (inner_milestones.has_key(m.id) == False):
                            inner_milestone = InnerMilestone(m.id, m.name, m.hard_data)
                            inner_milestones[m.id] = inner_milestone

                        # запоминаем решения, где содержится данный этап
                        if (solution_ids.has_key(m.id) == False):
                            solution_ids[m.id] = []
                        
                        arr = solution_ids[m.id]
                        arr.append(solution.id)
                        solution_ids[m.id] = arr
                                                
        if (len(inner_milestones) > 1):
            result = 1
            milestone_id = get_filtered_milestone_id(an, inner_milestones, attempt, solution_ids, possible_milestones, real_combs)
            
        elif (len(inner_milestones) == 1):
            milestone_id = inner_milestones.values()[0].id
            
        if (milestone_id > 0):                
            milestone = Milestone.objects.get(id=milestone_id)
                                                                                                                          
    else:       
        milestone = Milestone.objects.get(id=milestone_id)
        
    if (milestone is not None):
        #для логирования
        milestone_id = milestone.id
        # на всякий случай
        result = an.check_equivalence(notations, step_data, milestone.hard_data)
        if (result == 1):
            required_expression = get_expression(an, milestone, step_data, attempt)
            
            student_steps=Step.objects.filter(attempt=attempt, status=1).order_by('time')
            solutions = Solution.objects.filter(task=task) 
            combs = Milestone.objects.filter(solution__in=solutions, is_multiple=True).distinct().values('id')
            
            #ищем нереализованные комбинаторные этапы
            unreal = []
            for i in range(len(combs)):
                if (not(combs[i] in real_combs)):
                    unreal.append(combs[i])
                    
            if (milestone.is_multiple == True):
                real_combs.append(milestone.id) 
            else:
                if (len(unreal) > 0):
                    alters_ids = Milestone.objects.filter(solution__in=Solution.objects.filter(task=task, not_necessary=True)).values('id')
                    #присутствует ли проверяемый этап в решениях "с необязательными" этапами
                    in_alters = False
                    for val in alters_ids:
                        if (val['id'] == milestone.id):
                            in_alters = True
                    if (in_alters):
                        equiv_rels = EquivRel.objects.filter(task=task)
                        arr = []
                        for equiv_rel in equiv_rels:
                            equiv_milestones_ids = equiv_rel.equivalent_milestones.values('id')
                            add_to_arr = False
                            arr2 = []
                            for j in range(len(equiv_milestones_ids)):
                                el = equiv_milestones_ids[j]['id']
                                if (el == milestone.id):
                                    add_to_arr = True
                                arr2.append(el)
                            if (add_to_arr):
                                arr.append(arr2)                             
                        stud_milestone_ids =  get_stud_milestones(student_steps, possible_milestones, real_combs) 
                        stud_milestone_ids.append(milestone.id)
                        comb_ids = get_realized_comb_ids(arr, stud_milestone_ids)
                        if (len(comb_ids) > 0):
                            real_combs.extend(comb_ids)                     
                    
    result=save_step_hard_interface(attempt, number, milestone, hard, step_data, result, required_expression) 
    
    st_inner_milestones = map(str, inner_milestones)
            
    data = {
            'result': str(result),
            'inner_milestones': st_inner_milestones,
            'milestone_id': milestone_id,
            'real_combs' : real_combs,
    }
    s = json.dumps(data) 
        
    return HttpResponse(s)

def get_realized_comb_ids(arr, stud_milestone_ids):
    '''вспомогательная функция, которая если находит, что в решении студента реализован некоторый комбинаторный этап, возвращает его id'''
    #здесь смотрим, все ли этапы из эквивалентности реализованы в решении студента. Если не реализован только один, то это кобминаторный
    #этап, его id и возвращаем
    id = []
    s = 0
    for i in range(len(arr)):
        arr2 = arr[i]
        k = 0
        for j in range(len(arr2)):
            if (arr2[j]  in stud_milestone_ids):
                k = k + 1
            else:
                s = arr2[j]
        if (k == len(arr2) - 1):
            id.append(s)
    return id
        

def get_expression(an, milestone, step_data, attempt):
    '''подбирает наиболее похожее на ответ студента обязательное выражение (форму ответа) из заданных преподавателем
    для данного шага'''
    notations = [task_notation.notation.name for task_notation in attempt.task.task_notations.all()]
    required_expression = None
    required_expressions_set = Expression.objects.filter(milestone=milestone)
    if required_expressions_set.count() > 0:
        req = {}
        req_arr = {}
        for expression in required_expressions_set:
            req_arr[expression]=[an.op_diff(step_data, expression.text), an.val_diff(step_data, expression.text)]
        
        sum1 = 0 
        sum2 = 0
        for key, value in req_arr.iteritems():
            arr = req_arr[key]
            sum1 = sum1 + arr[0]
            sum2 = sum2 + arr[1]
            
        min_val = 1000

        for key, value in req_arr.iteritems():
            arr = req_arr[key]
            if (sum1 > 0):
                arr[0] = arr[0]/sum1
            if (sum2 > 0):
                arr[1] = arr[1]/sum2
            diff = 0.35*arr[0] + 0.65*arr[1]
            if (diff < min_val):
                min_val = diff
                required_expression = key
                
        return required_expression
    
def complain(request):
    attempt = Attempt.objects.get(id=int(request.session['attempt']))
    steps = Step.objects.filter(attempt=attempt).order_by('time')
    step = steps[steps.count()-1]
    step.status = 0
    step.save()
    return HttpResponse('')

def save_step_hard_interface(attempt, number, milestone, hard, step_data, result, required_expression):
    step = Step.objects.create(attempt=attempt)
    step.attempt = attempt
    step.number = number
    step.milestone = milestone
    if (hard == 1):
        step.interface = 1
        step.data_hard = step_data
    else:
        step.data_easy = step_data
        step.interface = 0
    if (result == 1): 
        step.status = 1
        result = step.id
    else:
        step.status = -1
    if required_expression:
        step.required_expression = required_expression
    step.save() 
     
    return result

def save_step_easy_interface(request):
    attempt = Attempt.objects.get(id=int(request.session['attempt']))
    an = Analyzer()
    notations = [task_notation.notation.name for task_notation in attempt.task.task_notations.all()]
    an.set_notations(notations)
    milestone = request.GET.get('milestone', 0)
    number = request.GET.get('number', 0)
    data = request.GET.get('data', 0)
    if (data.find('pmatrix') != -1):
        data = data.replace('\\begin{pmatrix}','[')
        data = data.replace('\\end{pmatrix}',']')
        data = data.replace('&',',')
        data = data.replace('\\\\',';')
    required_expression = get_expression(an, milestone, data, attempt)
    result = request.GET.get('result', 0)
    milestone = Milestone.objects.get(id=int(milestone))
    step = Step.objects.create(attempt=attempt)
    step.attempt = attempt
    step.number = number
    step.milestone = milestone
    step.data_easy = data
    step.interface = 0
    step.required_expression = required_expression
    step.status = result
    step.save()
    result = str(step.id)
    
    return HttpResponse(result)   

def remove_step(request, step_id):
    """ Удаляем шаг и следующие за ним """
    try:
        step = Step.objects.get(id=int(step_id))
        attempt = step.attempt
        next_steps = Step.objects.filter(number__gt=step.number, attempt=attempt)
        next_steps.delete()
        step.delete()
    except Exception:
        pass
    return HttpResponse('')

def modify_step(request, step_id):
    """ Модифицируем статус шага при изменении """
    try:
        step = Step.objects.get(id=int(step_id))
        step.status = 0
        step.save()
    except Exception:
        pass
    return HttpResponse('')

@csrf_exempt
def write_log(request):
    """ Записываем в лог """
    data = request.POST.get('data', '')
    attempt_id = 0
    
    try:
        attempt_id = int(request.session['attempt'])
        #нет открытых попыток решения задач
        if (attempt_id == -1):
            return HttpResponse('')          
    except:
        return HttpResponse('')
    
    session = Session.objects.get(id=int(request.session['student_session_id']))    
    attempt = Attempt.objects.get(id=attempt_id)

    try:
        data = json.loads(data)
    except Exception:
        return HttpResponseBadRequest('Empty data or bad format')

    for item in data:
        #log = Log(log_data=item['data'], log_time=item['time'])
        log = Log(log_data=item['data'], log_time=datetime.datetime.fromtimestamp(item['time']/1000.0))
        log.session = session
        log.save()

    return HttpResponse('')

def xstr(s):
    if s is None:
        return ''
    return smart_str(s)

# получаем шаблон для легкого интерфейса

def get_easy_template(request, milestone_id):

    try:
        easy_interface_setting = get_easy_interface_setting(request, milestone_id)
        data = {
            'template': xstr(easy_interface_setting.view),
            'variables': xstr(easy_interface_setting.variables),
            'check_type': xstr(easy_interface_setting.check_type),
        }

    except BaseException as e:
        data = {'template': e}
    s = json.dumps(data)

    return HttpResponse(s)

def get_easy_interface_setting(request, milestone_id):

    milestone = Milestone.objects.get(id=int(milestone_id))
    attempt = Attempt.objects.get(id=int(request.session['attempt']))
    easy_interface_setting = None
    required_expressions_set = Expression.objects.filter(milestone=milestone)    
    if (required_expressions_set.count() > 0):
        inputted_required_expressions_set = Expression.objects.filter(step__milestone=milestone, step__attempt=attempt)
        inputted_required_expressions_ids = [p.id for p in inputted_required_expressions_set]
        # получаем еще не введенные в правильных шагах обязательные выражения данного шага
        not_inputted_required_expressions_set = Expression.objects.filter(milestone=milestone).exclude(id__in=inputted_required_expressions_ids).order_by('id')
        if (not_inputted_required_expressions_set.count() > 0):
            easy_interface_setting = not_inputted_required_expressions_set[0].easy_interface_setting
    else:
        easy_interface_setting = Easy_interface_setting.objects.get(milestone=milestone)

    return easy_interface_setting

# получаем данные для проверки легкого интерфейса

def get_easy_data_check_data(request, milestone_id):
    
    milestone = Milestone.objects.get(id=int(milestone_id))
    mimetype = 'text/html'
    easy_interface_setting = get_easy_interface_setting(request, milestone_id)
    
    # Если возвращается скрипт для проверки, то говорим, что передаем javascript-код
    if easy_interface_setting.check_type == 3:
        mimetype = 'text/javascript'
        
    data = {
            'check_data': xstr(easy_interface_setting.check_data),
            'formula_view': xstr(easy_interface_setting.formula_view),
            'check_type': xstr(easy_interface_setting.check_type),
    }
    s = json.dumps(data)
        
    return HttpResponse(s, mimetype=mimetype)

#получаем список идентификаторов этапов, от которых зависит проверка текущего этапа

def get_easy_data_depend_on(request, milestone_id):
    result = '';
    milestone = Milestone.objects.get(id=int(milestone_id))
    depends_on = milestone.depends_on.all();
    for d in depends_on:
        result = result + str(d.id) + ';;;';
    #обрезать строку
    return HttpResponse(result)

def update_solution(request, step_id):
    step = Step.objects.get(id=int(step_id))    
    return render_to_response("entered_step.html", {'step': step})    


#============================= Debug ================================

def show_filtered_hints(request):
    task_id = int(request.GET.get('task_id', 0))
    hint_type = int(request.GET.get('hint_type', 0))
    if (task_id == 0 or hint_type == 0):
        return HttpResponse('')
    task = Task.objects.get(id=task_id)
    solutions = Solution.objects.filter(task=task)
    milestones = Milestone.objects.filter(solution__in=solutions).distinct()    
    return render_to_response("debug_hints_list.html", {'milestones': milestones, 'hint_type': hint_type})

def show_all_hints(request):
    tasks = Task.objects.all()
    return render_to_response("select_hints.html", {'tasks': tasks})

def show_filtered_solutions(request):
    task_id = int(request.GET.get('task_id', 0))
    if (task_id == 0):
        return HttpResponse('')
    task = Task.objects.get(id=task_id)
    solution = Solution.objects.filter(task=task)
    solution_milestones= SolutionMilestone.objects.filter(solution__in=solution).order_by('solution')
    return render_to_response("debug_solutions_list.html", {'solution_milestones': solution_milestones})

def show_all_solutions(request):
    tasks = Task.objects.all()
    return render_to_response("select_solutions.html", {'tasks': tasks})

def show_all_complaints(request):
    students = Student.objects.all().prefetch_related('sessions__attempt_set__step_set')
    #students = Student.objects.all()
    return render_to_response("complaints_list.html", {'students': students})