# coding: utf-8
from students.models import Session, Attempt
from django.http import HttpResponse
from tasks.views import get_filtered_hint_id, get_plan_hint_milestone_id
import json
import string
from tasks.utils import get_int_arr_from_GET

def suggest_help(request):
    u''' функция, которая в зависимости от прогресса студента за проверяемый период может рекомендовать ему воспользоваться каким-либо видом помощи '''
    progress = int(request.GET.get('progress', 0))
    last_correct = int(request.GET.get('last_correct', -1))
    hard_interface = bool(int(request.GET.get('hard_interface', 0)))
    attempt = Attempt.objects.get(id=int(request.session['attempt']))
    hints_ids = request.GET.get("ids", "")
    milestone_id = request.GET.get('milestone_id', 0)
    possible_milestones = get_int_arr_from_GET('possible_milestones', request)
    combinatorial_milestones = get_int_arr_from_GET('combinatorial_milestones', request)
    situations_array = get_int_arr_from_GET('situations', request)
    
    selected_help = u''
    message = u''
    situation = 0
                                                  
    # ситуация1
    if ((progress == 2 or progress == 3) and last_correct > 2):  
        hint_id = get_filtered_hint_id(hints_ids, milestone_id, not(hard_interface), 1, 2)
        if (hint_id > 0):
            message = u'После чтения теоретического материала вы можете продвинуться в решении задачи. Попробуете почитать?'
            selected_help = 'theory'
            situation = 1

    # ситуация 2
    elif (progress == 1 and last_correct <= 2):
        # сведения о том, есть ли эти подсказки в наличии
        candidates_existence = {}
        
        milestone_id = get_plan_hint_milestone_id(attempt, possible_milestones, combinatorial_milestones)
        if (milestone_id > 0):
            candidates_existence['plan_hint'] = 1
        else:
            candidates_existence['plan_hint'] = 0
        
        hint_id = get_filtered_hint_id(hints_ids, milestone_id, not(hard_interface), 1, 1)
        if (hint_id > 0):
            candidates_existence['theoretic'] = 1
        else:
            candidates_existence['theoretic'] = 0
        
        hint_id = get_filtered_hint_id(hints_ids, milestone_id, not(hard_interface), 2, 1)
        if (hint_id > 0):
            candidates_existence['schematic'] = 1
        else:
            candidates_existence['schematic'] = 0
        
        #сведения о приоритете показа подсказок. Если ситуация возникает много раз, то подсказки чередуются по кругу
        candidates_priority = []
        candidates_priority.append('plan_hint')
        candidates_priority.append('theoretic')
        candidates_priority.append('schematic') 
        
        num = situations_array[1]
        if (num >= 3):
            num = situations_array[1] % 3
            if (num == 0):
                num = 2
        
        found = False
        i = 1
        while (found == False and i <= 3):
            hint = candidates_priority[num]
            if (candidates_existence[hint] == 1):
                found = True
                selected_help = hint
            i = i + 1
            
        if (selected_help == 'plan_hint'):
            message = u'Вы можете воспользоваться подсказкой по ходу решения. Примете помощь?'
            
        elif (selected_help == 'theoretic'):
            message = u'Вы можете воспользоваться краткой теоретической подсказкой по текущему шагу. Примете помощь?'
            
        elif (selected_help == 'schematic'):
            message = u'Вы можете воспользоваться краткой схематической подсказкой по текущему шагу. Примете помощь?'               
                
        situation = 2
        
    # ситуация 3
    elif ((progress == 1 and last_correct > 2) or progress == 0):

        candidates_existence = {}
        
        if (hard_interface):
            candidates_existence['change_interface'] = 1
        else:
            candidates_existence['change_interface'] = 0
            
        hint_id = get_filtered_hint_id(hints_ids, milestone_id, not(hard_interface), 3, 1)
        if (hint_id > 0):
            candidates_existence['concrete'] = 1
        else:
            candidates_existence['concrete'] = 0
            
        hint_id = get_filtered_hint_id(hints_ids, milestone_id, not(hard_interface), 1, 2)
        if (hint_id > 0):
            candidates_existence['theory'] = 1
        else:
            candidates_existence['theory'] = 0
            
        hint_id = get_filtered_hint_id(hints_ids, milestone_id, not(hard_interface), 3, 3)
        if (hint_id > 0):
            candidates_existence['change_task'] = 1
        else:
            candidates_existence['change_task'] = 0
            
        situation = 3
        
        candidates_priority = []
        candidates_priority.append('change_interface')
        candidates_priority.append('concrete')
        candidates_priority.append('theory') 
        candidates_priority.append('change_task') 
        
        num = situations_array[2]
        if (num >= 4):
            num = situations_array[2] % 4
            if (num == 0):
                num = 3
        
        found = False
        i = 1
        while (found == False and i <= 4):
            hint = candidates_priority[num]
            if (candidates_existence[hint] == 1):
                found = True
                selected_help = hint
            i = i + 1
            
        if (selected_help == 'change_interface'):
            message = u'Вы можете переключиться на легкий интерфейс. Примете помощь?'
            
        elif (selected_help == 'concrete'):
            message = u'Вы можете воспользоваться конкретной подсказкой. Примете помощь?'            
            
        elif (selected_help == 'theory'):
            message = u'После чтения теоретического материала вы можете продвинуться в решении задачи. Попробуете почитать?'            
            
        elif (selected_help == 'change_task'):
            message = u'Вы можете переключиться на более простую задачу. Примете помощь?'
                        
    data = {
            'message': message,
            'selected_help': selected_help,
            'situation': situation,
    }
    s = json.dumps(data)      
        
    return HttpResponse(s)
        