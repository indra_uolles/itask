# coding: utf-8

from django.db import models
from django.contrib import admin
from django.forms.models import modelformset_factory
from django.forms import TextInput
from django import forms
from support.widgets import CKEditorWidget

from tasks.models import Task, Solution, Hint, Milestone, Milestone_type, Notation, Expression, Easy_interface_setting, TaskNotation, SolutionMilestone, EquivRel

class SolutionInLine(admin.StackedInline):
    filter_vertical = ('milestones', )
    model = Solution
    extra = 1

class TaskNotationInLine(admin.StackedInline):
    model = TaskNotation
    extra = 1
       
class SolutionMilestoneInLine(admin.StackedInline):
    model = SolutionMilestone
    extra = 1

class TaskAdminForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())
    class Meta:
        models = Task

class TaskAdmin(admin.ModelAdmin):
    inlines = [TaskNotationInLine, SolutionInLine]
    form = TaskAdminForm

class MilestoneInLine(admin.StackedInline):
    formset = modelformset_factory(Milestone)
    model = Milestone
    extra = 1

class SolutionAdmin(admin.ModelAdmin):
    inlines = [SolutionMilestoneInLine]
    filter_vertical = ('milestones', )
       
class EquivRelAdmin(admin.ModelAdmin):
    filter_vertical = ('equivalent_milestones', )

class HintInLine(admin.StackedInline):
    model = Hint
    extra = 1

class ExpressionInLine(admin.StackedInline):
    model = Expression
    extra = 1

class MilestoneAdmin(admin.ModelAdmin):
    inlines = [HintInLine, ExpressionInLine]
    filter_vertical = ('depends_on', )

class MilestineTypeAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'style': 'width: 600px'})}
    }


class HintAdminForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Hint

class HintAdmin(admin.ModelAdmin):
    model = Hint
    form = HintAdminForm

admin.site.register(Task, TaskAdmin)
admin.site.register(Solution, SolutionAdmin)
admin.site.register(Milestone, MilestoneAdmin)
admin.site.register(EquivRel, EquivRelAdmin)
admin.site.register(Milestone_type, MilestineTypeAdmin)
admin.site.register(Notation)
admin.site.register(Hint, HintAdmin)
admin.site.register(Easy_interface_setting)
admin.site.register(TaskNotation)