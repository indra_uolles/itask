# coding: utf-8
from students.models import Saving
import json
import string

def get_hint_save(hint, student):
    sessions = [ s for s in student.sessions.all() ]
    attempts = [attempt for attempt in hint.hint_task.attempts.filter(session__in=sessions)]

    savings = Saving.objects.filter(attempt__in=attempts)

    if savings.count() > 0:
        return savings[savings.count()-1]
    else:
        return None
    
def get_int_arr_from_GET(arr_name, request):
    '''функция возвращает массив целых чисел из request, где массив задан как x,x,x'''
    arr = []
    st = request.GET.get(arr_name, "")
    for s in string.split(st, ","):
        if (len(s) > 0):
            arr.append(int(s))
    return arr