# coding: utf-8
__author__ = 'toly'

import re
import string
from django import template

register = template.Library()


@register.filter(name='math_filter')
def math_filter(result, params=None):

    if params:
        params = string.split(params, ';')
        for param in params:
            try:
                p = string.split(param, '=')
                a, b = p[0], p[1]
                result = string.replace(result, '{%s}' % a, b)
            except Exception:
                pass

    return result

@register.filter(name='hint_filter')
def hint_filter(result, params=None):

    data = result
    if re.match(r'^http://[a-zA-Z0-9\/\?\.\-\=]+$', result):
        data = 'Рекомендуем почитать <a href="%s">этот материал</a>.'

    return data