# coding: utf-8
import json
from django.test import TestCase
# Будем обращаться к ссылкам по их имени
from django.core.urlresolvers import reverse
#from django.contrib.auth.models import User

from tasks.models import Task


class TasksTest(TestCase):
    fixtures = ['all_data.json']

    def testLog(self):
        """ Тестируем, записываются ли логи """
        # 1. Сначала зайдем пользователем
        #User.objects.create_user(username='test_user', password='123456')
        self.client.login(username='anya', password='123')

        # 2. Начнем попытку, зайдя по ссылке задачи
        # Получим id последней добавленной задачи
        last_task_id = Task.objects.all()[0].id
        self.client.get(reverse('solve_task', args=(last_task_id,)))

        # 3. Запишем лог в рамках попытки
        send_data = [{'data': 'Log message', 'time': '10:50:59'}]
        response = self.client.post(reverse('write_log'), {'data': json.dumps(send_data)})
        # И проверим, успешно ли он записался
        self.assertEqual(response.status_code, 200)
