from django.shortcuts import render_to_response
from django.http import Http404
from support.models import Support

def show_support(request, support_id):

    try:
        support = Support.objects.get(id=support_id)
    except Support.DoesNotExist:
        return Http404

    return render_to_response("support.html", {'support': support, })