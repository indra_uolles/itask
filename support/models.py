# coding: utf-8
from django.db import models

# Create your models here.
class Support(models.Model):
    title = models.CharField(max_length=300, verbose_name='Название')
    text = models.TextField(verbose_name='Содержание')

    def __unicode__(self):
        return self.title

class Theory(models.Model):
    title = models.CharField(max_length=300, verbose_name='Название')
    text = models.TextField(verbose_name='Содержание')

    def __unicode__(self):
        return self.title