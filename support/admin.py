from django.contrib import admin
from django.db import models
from django import forms
from django.forms import TextInput
#from ckeditor.widgets import CKEditorWidget
from .widgets import CKEditorWidget

from .models import Support


class SupportAdminForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Support

class SupportAdmin(admin.ModelAdmin):
    form = SupportAdminForm
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'style': 'width: 600px'})}
    }

admin.site.register(Support, SupportAdmin)


