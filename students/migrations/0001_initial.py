# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Student'
        db.create_table('students_student', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
        ))
        db.send_create_signal('students', ['Student'])

        # Adding model 'Session'
        db.create_table('students_session', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('identificator', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('date', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(related_name='sessions', to=orm['students.Student'])),
        ))
        db.send_create_signal('students', ['Session'])

        # Adding model 'Attempt'
        db.create_table('students_attempt', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('session', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['students.Session'])),
            ('task', self.gf('django.db.models.fields.related.ForeignKey')(related_name='attempts', to=orm['tasks.Task'])),
            ('task_parameters', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('help_enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('time_start', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('time_end', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('grade', self.gf('django.db.models.fields.FloatField')(default=0, null=True, blank=True)),
        ))
        db.send_create_signal('students', ['Attempt'])

        # Adding M2M table for field hints on 'Attempt'
        db.create_table('students_attempt_hints', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('attempt', models.ForeignKey(orm['students.attempt'], null=False)),
            ('hint', models.ForeignKey(orm['tasks.hint'], null=False))
        ))
        db.create_unique('students_attempt_hints', ['attempt_id', 'hint_id'])

        # Adding model 'Log'
        db.create_table('students_log', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('log_type', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('log_data', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('session', self.gf('django.db.models.fields.related.ForeignKey')(related_name='logs', to=orm['students.Session'])),
        ))
        db.send_create_signal('students', ['Log'])

        # Adding model 'Step'
        db.create_table('students_step', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('number', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('milestone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tasks.Milestone'], null=True, blank=True)),
            ('attempt', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['students.Attempt'])),
            ('interface', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('data_hard', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('data_easy', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('required_expression', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tasks.Expression'], null=True, blank=True)),
        ))
        db.send_create_signal('students', ['Step'])

        # Adding model 'Saving'
        db.create_table('students_saving', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('attempt', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['students.Attempt'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('students', ['Saving'])


    def backwards(self, orm):
        # Deleting model 'Student'
        db.delete_table('students_student')

        # Deleting model 'Session'
        db.delete_table('students_session')

        # Deleting model 'Attempt'
        db.delete_table('students_attempt')

        # Removing M2M table for field hints on 'Attempt'
        db.delete_table('students_attempt_hints')

        # Deleting model 'Log'
        db.delete_table('students_log')

        # Deleting model 'Step'
        db.delete_table('students_step')

        # Deleting model 'Saving'
        db.delete_table('students_saving')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'students.attempt': {
            'Meta': {'object_name': 'Attempt'},
            'grade': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'help_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hints': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['tasks.Hint']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'session': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['students.Session']"}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'task': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'attempts'", 'to': "orm['tasks.Task']"}),
            'task_parameters': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'time_end': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'time_start': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'students.log': {
            'Meta': {'object_name': 'Log'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_data': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'log_type': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'session': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'logs'", 'to': "orm['students.Session']"})
        },
        'students.saving': {
            'Meta': {'object_name': 'Saving'},
            'attempt': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['students.Attempt']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'students.session': {
            'Meta': {'object_name': 'Session'},
            'date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificator': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sessions'", 'to': "orm['students.Student']"})
        },
        'students.step': {
            'Meta': {'object_name': 'Step'},
            'attempt': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['students.Attempt']"}),
            'data_easy': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'data_hard': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interface': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'milestone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Milestone']", 'null': 'True', 'blank': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'required_expression': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Expression']", 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'students.student': {
            'Meta': {'object_name': 'Student'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'tasks.easy_interface_setting': {
            'Meta': {'object_name': 'Easy_interface_setting'},
            'check_data': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'check_type': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'formula_view': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'variables': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'view': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        'tasks.expression': {
            'Meta': {'object_name': 'Expression'},
            'easy_interface_setting': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Easy_interface_setting']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'milestone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Milestone']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        'tasks.hint': {
            'Meta': {'ordering': "['hint_type', 'idx']", 'object_name': 'Hint'},
            'for_easy': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'hint_kind': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'hint_type': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idx': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'milestone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Milestone']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'tasks.milestone': {
            'Meta': {'object_name': 'Milestone'},
            'depends_on': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'depends_on_rel_+'", 'null': 'True', 'to': "orm['tasks.Milestone']"}),
            'easy_interface_setting': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Easy_interface_setting']", 'null': 'True', 'blank': 'True'}),
            'hard_data': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_multiple': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'plan_hint': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tasks.Milestone_type']", 'null': 'True', 'blank': 'True'})
        },
        'tasks.milestone_type': {
            'Meta': {'object_name': 'Milestone_type'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'tasks.task': {
            'Meta': {'ordering': "['name']", 'object_name': 'Task'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['students']