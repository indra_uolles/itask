# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from students.models import Session, Attempt, Step, Student, Log, Saving


# Define an inline admin descriptor for UserProfile model
# which acts a bit like a singleton
class StudentInline(admin.StackedInline):
    model = Student
    can_delete = False
    verbose_name_plural = u'Студент'


# Define a new User admin
class StudentAdmin(UserAdmin):
    inlines = (StudentInline, )


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, StudentAdmin)

admin.site.register(Session)
admin.site.register(Attempt)
#admin.site.register(Student)
admin.site.register(Step)
admin.site.register(Log)
admin.site.register(Saving)
