# coding: utf-8

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

from tasks.models import Task, Hint, Milestone, Expression


# Now this is default Profile class
class Student(models.Model):
    user = models.OneToOneField(User, unique=True)

    def __unicode__(self):
        return str(self.user.username)


def create_user_profile(sender, instance, created, **kwargs):
    """ This code is also for replacement default auth.User to students.Student """
    if created:
        Student.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)


class Session(models.Model):
    
    identificator = models.CharField(max_length=255, verbose_name='Идентификатор сессии')
    date = models.DateField(auto_now_add=True)
    student = models.ForeignKey(Student, related_name='sessions', verbose_name='Студент')
    
    def add_to_log(self, data, time, log_type=''):
        self.save()
        log = Log(log_data=data, log_time=time, log_type=log_type, session=self)
        log.save()

class Attempt(models.Model):
    STATUS_CHOICES = [
        (-1, 'fail'),
        (1, 'success'),
        (0, 'in_progress'),
    ]

    status = models.IntegerField(choices=STATUS_CHOICES, default=0, verbose_name='Статус')
    session = models.ForeignKey(Session, verbose_name='Сессия')
    task = models.ForeignKey(Task, related_name='attempts', verbose_name='Задача')
    task_parameters = models.TextField(blank=True, null=True, verbose_name='Параметры')
    hints = models.ManyToManyField(Hint, blank=True, verbose_name='Взятые подсказки')
    help_enabled = models.BooleanField(default=False)
    time_start = models.DateTimeField(auto_now_add=True)
    time_end = models.DateTimeField(auto_now=True)
    grade = models.FloatField(blank=True, null=True, default=0)

class Log(models.Model):
    log_type = models.CharField(max_length=255, blank=True, null=True)
    log_time = models.DateTimeField()
    log_data = models.CharField(max_length=512)
    session = models.ForeignKey(Session, related_name='logs')

class Step(models.Model): 
    STATUS_CHOICES = [
        (-1, 'fail'),
        (1, 'success'),
        (0, 'in_progress'),
    ]

    INTERFACE_CHOICES = [
        (1, 'hard'),
        (0, 'easy'),
    ]

    time = models.DateTimeField(auto_now=True)
    number = models.IntegerField(default=0)
    status = models.IntegerField(choices=STATUS_CHOICES, default=0, verbose_name='Статус')
    milestone = models.ForeignKey(Milestone, blank=True, null=True, verbose_name='Этап')
    attempt = models.ForeignKey(Attempt)
    interface = models.IntegerField(choices=INTERFACE_CHOICES, default=0, verbose_name='Интерфейс')
    data_hard = models.TextField(blank=True, null=True)
    data_easy = models.TextField(blank=True, null=True)
    required_expression = models.ForeignKey(Expression, blank=True, null=True)

    def __unicode__(self):
        return u'Шаг ' + str(self.id)

class Saving(models.Model):
    attempt = models.ForeignKey(Attempt)
    date = models.DateTimeField(auto_now_add=True)