# coding: utf-8

# Create your views here.
import string
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from students.models import Session, Attempt, Student, Step, Log, Saving
from tasks.models import Milestone
from datetime import datetime

# выводим попытки
def all_attempts(request):
    attempts = Attempt.objects.all()
    return render_to_response("attempts.html", {'attempts': attempts, })

# выводим сессии
def all_logs(request):
    sessions = Session.objects.all()
    return render_to_response("logs.html", {'sessions': sessions, })

# попытки студента
def student_attempts(request, student_id):
    student = Student.objects.get(id=int(student_id))
    sessions = Session.objects.filter(student=student)
    attempts = Attempt.objects.filter(session__in=sessions)
    return render_to_response("sessions.html", {'attempts': attempts, })

# попытки в течении сессии
def session_attempts(request, session_id):
    session = Session.objects.get(id=session_id)
    attempts = Attempt.objects.filter(session=session)
    return render_to_response("attempts.html", {'attempts': attempts, })

# удалить попытку и сессии без попыток
@csrf_exempt
def remove_attempt(request):
    ids = request.POST.get('ids', '')
    if ids:
        ids = string.split(ids, ',')
        ids = [int(id) for id in ids]
        attempts = Attempt.objects.filter(id__in=ids)
        sessions = [attempt.session for attempt in attempts]
        sessions = list(set(sessions))
        attempts.delete()
        for session in sessions:
            if Attempt.objects.filter(session=session).count() == 0:
                session.delete()

    return HttpResponse('')

#удалить выбранные логи
def remove_logs(request):
    ids = request.POST.get('ids', '')
    if ids:
        ids = string.split(ids, ',')
        ids = [int(id) for id in ids]
        sessions = Session.objects.filter(id__in=ids)
        logs = Log.objects.filter(session__in=sessions)
        logs.delete()

    return HttpResponse('')

# показываем попытку
def show_attempt(request, attempt_id):
    attempt = Attempt.objects.get(id=int(attempt_id))
    steps = Step.objects.filter(attempt=attempt)
    task_id = attempt.task.id
    str_filter = 'RECEIVE_HINT ' + str(task_id)
    logs = Log.objects.filter(session=attempt.session, log_data__contains=str_filter)
    return render_to_response("attempt.html", {'attempt': attempt, 'steps': steps, 'logs': logs})

# показываем лог сессии
def show_logs(request, session_id):
    session = Session.objects.get(id=int(session_id))
    logs =Log.objects.filter(session=session)
    return render_to_response("log.html", {'logs': logs, 'student_name': session.student.user.username, 'identificator': session.identificator})

#закрываем попытку
def close_attempt(request, grade):
    try:
        attempt = Attempt.objects.get(id=int(request.session['attempt']))
        if (float(grade) >= 100):
            attempt.status = 1            
        else:
            attempt.status = -1
        attempt.grade = float(grade)
        attempt.time_end = datetime.now().strftime('%Y/%m/%d')
        attempt.save()
        session = Session.objects.get(id=int(request.session['student_session_id']))
        attempts = Attempt.objects.filter(session=session, status=0).order_by('time_start')  
        if (attempts.count() == 0):
            request.session['attempt']=attempts[attempts.count()-1].id
        else:
            request.session['attempt']=-1
            
    except Exception, e:
        print e
    return HttpResponse('ok') 

#возобновляем попытку
def revive_attempt(request, attempt_id):  
    try:
        attempt = Attempt.objects.get(id=int(attempt_id))
        attempt.status = 0
        attempt.time_end = datetime.now().strftime('%Y/%m/%d')
        attempt.save()
        request.session['attempt'] = attempt.id
    except Exception, e:
        print e
    return HttpResponse('ok')      

# сохраняем решение - SAVE
@csrf_exempt
def save_attempt(request):
    try:
        attempt = Attempt.objects.get(id=int(request.session['attempt']))
        saving = Saving(attempt=attempt)
        saving.save()
    except Exception, e:
        print e
    return HttpResponse('ok')

# SAVE - показ сохраненной попытки
def hint_attempt(request, attempt_id):
    attempt = Attempt.objects.get(id=attempt_id)
    student = attempt.session.student
    task = attempt.task
    task_params = attempt.task_parameters
    milestones = Milestone.objects.all()

    request.session['attempt'] = attempt.id

    steps = Step.objects.filter(attempt=attempt)

    data = {
        'task': task,
        'student_id': student.id,
        'milestones': milestones,
        'help': attempt.help_enabled,
        'task_params': task_params,
        'steps': steps,
        }

    return render_to_response("task.html", data)