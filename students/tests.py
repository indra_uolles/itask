# coding: utf-8
from django.test import TestCase
# Будем обращаться к ссылкам по их имени
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from students.models import Student


class UserTest(TestCase):
    def testUrls(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)

    def testCustomUserProfile(self):
        """
            Тестируем удачно ли мы расширили обычную джанговскую
            модель для аутентификации моделью студента
        """
        user = User.objects.create_user(username='test_user', password='123456')
        student = Student.objects.get(user=user)
        self.assertEqual(student.user.id, user.id)

    def testLogin(self):
        """
            Тестируем вход в систему
        """
        User.objects.create_user(username='test_user', password='123456')
        response = self.client.post(reverse('login'),
        {'username': 'test_user', 'password': '123456'})
        self.assertEqual(response.status_code, 302)  # Перенаправляет, если вход успешен
