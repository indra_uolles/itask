Django 1.4.22

Для запуска на timeweb, например, нужено:
— public_html/index.wsgi 
— public_html/.htaccess 
— public_html/static -> public_html/itask/main/static
— public_html/templates -> public_html/itask/main/templates


index.wsgi такого толка:
```
import os, sys

sys.path.append('/home/e/erastov/itask.sky2high.net/public_html/itask')
sys.path.append('/home/e/erastov/itask.sky2high.net/public_html/site-packages')

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
```

.htaccess:
```
Options +ExecCGI
AddHandler wsgi-script .wsgi
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.*)$ /index.wsgi/$1 [QSA,PT,L]
```

====================
Тут должно быть описание структуры проекта

ckeditor и gellerific (для отображения выбора файлов в админке) изменены:
1. ckeditor static files не в обычной папке, а в /static/lib/ckeditor
2. т.к. gallerific берет свои файлы только начиная с префикса settings.STATIC_DIR
пришлось руками изменить template в модуле ckeditor: /lib/ckeditor/templates/browse.html секция в <head></head>



============================ Изменения ========================================

28.11.2012 – Дмитрий
– Рефакторинг get_show_hints.js и еще где-то по мелочам
– Добавил в logging.add_to_log() функцию console.log(), что позволило убрать лишние
  console.log по всему js-коду.

03.11.2012 – Дмитрий
– Добавил анимацию правильного введенного шага
– Добавил анимацию проверки введенного шага
– Вынес эти анимации в отдельный файл task_animations.js
– Добавил крестики закрытия окон с подсказками и обозначениями

02.11.2012 – Дмитрий
– Убрал лишние setInterval(logger.send_log, ...)
– Местами привел форматирование кода к общепринятому виду
– В entered_step заменил <br /> на <br>, т.к. в приложении у нас везде html 5, а не xhtml 1.0. Впрочем, такое изменение ни на что не влияет.
– Добавил TODO в функцию change_step_status.
– @TODO: use RequireJS
– Added @FIXME on line 323 in check_answers.js
– Поправил шаблон task.html так, чтобы полоса прокрутки была на весь блок.



11.10.2012 – Дмитрий
–  Добавил тесты логирования, аутентификации
–  Небольшой рефакторинг низа main.js
–  Теперь при просмотре теории не загружаются лишние скрипты, которые используются только в решении задачи. Сделано это вынесом загрузки скриптов в шаблок task.html в блок {% block head %}. Необходимо было для правильного логирования просмотра теории.
–  Логирование теории
–  Что-то еще по мелочам
Как проводить тесты: ./manage.py test students tasks

28.09.2012 - Дмитрий
– Помимо улучшений theory, добавил Ок и анимацию в модальные окна.

19.09.2012 - Дмитрий
– Изменил способ получения id текущего пользователя в solve_task()
– Небольшие косметические улучшения

18.09.2012 - Дмитрий
– Для работы с пользователями необходимо добавить колонку students__studen.user_id в БД и удалить лишних Students без user_id.
– Переформатирован urls.py для разделения на помодульные секции
– Добавил вывод из шаблонов заголовков в <title></title> ({% block title%}{% endblock %})
– Обновил bootstrap
– Много изменение по аутентификации



